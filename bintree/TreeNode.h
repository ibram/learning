#pragma once
#include <cstddef>
#include <utility>

template<typename T>
class AVLTreeNode
{
private:
	typedef T ValueType;
	ValueType m_pair;
	unsigned char m_height;
	AVLTreeNode<T> *m_leftNode, *m_parentNode, *m_rightNode;

	void setParentNode(AVLTreeNode<T> *parentNode)
	{
		m_parentNode = parentNode;
	}

public:
	/*
	 * \brief Конструктор.
	 * \param key Ключ узла.
	 * \param value Значение узла.
	 */
	AVLTreeNode(const ValueType &pair):
		m_pair(pair)
	{
		m_height = 1;
		m_parentNode = m_leftNode = m_rightNode = NULL;
	}

	/*
	 * \brief Возвращает высоту узла.
	 * \return Высота узла.
	 */
	unsigned char getHeight() const
	{
		return m_height;
	}

	/* \brief Возвращает левый дочерний узел.
	 * \return Левый дочерний узел.
	 */
	AVLTreeNode<T>* getLeftNode() const
	{
		return m_leftNode;
	}

	ValueType &getPair()
	{
		return m_pair;
	}

	const ValueType &getPair() const
	{
		return m_pair;
	}

	AVLTreeNode<T> *getParentNode()
	{
		return m_parentNode;
	}

	const AVLTreeNode<T> *getParentNode() const
	{
		return m_parentNode;
	}

	/*
	 * \brief Возвращает правый дочерний узел.
	 * \return Правый дочерний узел.
	 */
	AVLTreeNode<T>* getRightNode() const
	{
		return m_rightNode;
	}

	void recalculateHeight()
	{
		unsigned char leftHeigth = 0;
		unsigned char rightHeigth = 0;

		if (m_leftNode != NULL)
			leftHeigth = m_leftNode->getHeight();

		if (m_rightNode != NULL)
			rightHeigth = m_rightNode->getHeight();

		if (leftHeigth > rightHeigth)
			m_height = leftHeigth + 1;
		else
			m_height = rightHeigth + 1;
	}

	void setLeftNode(AVLTreeNode<T> *node)
	{
		m_leftNode = node;

		if (node != NULL)
			node->setParentNode(this);
	}

	void setRightNode(AVLTreeNode<T> *node)
	{
		m_rightNode = node;

		if (node != NULL)
			node->setParentNode(this);
	}
};
