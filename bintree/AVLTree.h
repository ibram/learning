#pragma once
#include <functional>
#include "BidirectionalIterator.h"
#include "TreeNode.h"

template<typename T, typename X, typename T2 = std::less<T> >
class AVLTree
{
public:
	typedef AVLTreeNode<std::pair<T, X> > Node;
	typedef BidirectionalIterator<std::pair<T, X> > iterator;
	typedef std::reverse_iterator<iterator> reverse_iterator;

private:
	typedef std::pair<T, X> ValueType;
	typedef T KeyType;

	T2 less;
	Node *m_rootNode, *m_nodeAfterLast;
	unsigned int m_size;
	bool m_autoBalance, m_slowSource;

	void clear(Node *rootNode)
	{
		if (rootNode != 0)
		{
			if (rootNode->getRightNode() != 0)
				clear(rootNode->getRightNode());
			if (rootNode->getLeftNode() != 0)
				clear(rootNode->getLeftNode());

			delete rootNode;
		}
	}

	Node *insert(Node *rootNode, const ValueType &pair)
	{
		if (rootNode == NULL)
			return new Node(pair);

		if (less(pair.first, rootNode->getPair().first))
			rootNode->setLeftNode(insert(rootNode->getLeftNode(), pair));
		else
			rootNode->setRightNode(insert(rootNode->getRightNode(), pair));

		if (m_autoBalance)
			return balance(rootNode);

		rootNode->recalculateHeight();
		return rootNode;
	}

	Node *balance(Node *node)
	{
		if (node != NULL)
		{
			node->recalculateHeight();

			if (getNodeBalanceFactor(node) == 2)
			{
				if (getNodeBalanceFactor(node->getRightNode()) < 0)
					rotateRight(node->getRightNode());

				return rotateLeft(node);
			}

			if (getNodeBalanceFactor(node) == -2)
			{
				if (getNodeBalanceFactor(node->getLeftNode()) > 0)
					rotateLeft(node->getLeftNode());

				return rotateRight(node);
			}
		}
		return node;
	}

	Node *findMaxKeyNode(Node *node)
	{
		if (node == NULL || node->getRightNode() == NULL)
			return node;

		return findMaxKeyNode(node->getRightNode());
	}

	Node *findMinKeyNode(Node *node)
	{
		if (node == NULL || node->getLeftNode() == NULL)
			return node;

		return findMinKeyNode(node->getLeftNode());
	}

	Node *findNode(Node *node, const KeyType &key)
	{
		if (node == NULL)
			return NULL;

		//Корневой ключ равен запрошенному - нашли
		if (!less(node->getPair().first, key) && !less(key, node->getPair().first))
			return node;

		//Если запрошенный ключ больше корневого, ищем справа
		if (less(node->getPair().first, key))
		{
			if (m_slowSource)
			{
				makeDelay();
			}

			return findNode(node->getRightNode(), key);
		}

		if (m_slowSource)
		{
			makeDelay();
		}

		return findNode(node->getLeftNode(), key); //Иначе ищем слева
	}

	void fixAfterLastNode()
	{
		m_nodeAfterLast->setLeftNode(m_rootNode);
		m_nodeAfterLast->setRightNode(m_rootNode);
	}

	/* \brief Возвращает баланс узла.
	 * Баланс узла - разница между высотой правого поддерева и левого поддерева.
	 * \return Баланс узла.
	 */
	int getNodeBalanceFactor(const Node *node) const
	{
		if (node != NULL)
			return getNodeHeigth(node->getRightNode()) - getNodeHeigth(node->getLeftNode());

		return 0;
	}

	/* \brief Возвращает высоту узла.
	 * \return Высота узла.
	 */
	unsigned char getNodeHeigth(const Node *node) const
	{
		if (node != NULL)
			return node->getHeight();

		return 0;
	}

	Node *getMinKeyNode(Node *node)
	{
		if (node == NULL)
			return NULL;

		if (node->getLeftNode() == NULL)
			return node->getRightNode();

		node->setLeftNode(getMinKeyNode(node->getLeftNode()));

		if (m_autoBalance)
			return balance(node);

		return node;
	}

	Node *erase(Node *node, const KeyType &key)
	{
		if (less(key, node->getPair().first))
			node->setLeftNode(erase(node->getLeftNode(), key));
		else if (less(node->getPair().first, key))
			node->setRightNode(erase(node->getRightNode(), key));
		else
		{
			Node *leftNode = node->getLeftNode();
			Node *rightNode = node->getRightNode();
			delete node;

			if (rightNode == NULL)
				return leftNode;

			Node *minKeyNode = findMinKeyNode(rightNode);
			minKeyNode->setRightNode(getMinKeyNode(rightNode));
			minKeyNode->setLeftNode(leftNode);

			if (m_autoBalance)
				return balance(minKeyNode);

			minKeyNode->recalculateHeight();
			return minKeyNode;
		}

		if (m_autoBalance)
			return balance(node);

		return node;
	}

	Node *rotateLeft(Node *baseNode)
	{
		if (baseNode == NULL)
			return NULL;

		Node *rightNode = baseNode->getRightNode();
		baseNode->setRightNode(rightNode->getLeftNode());

		if (rightNode->getPair().first < baseNode->getParentNode()->getPair().first)
			baseNode->getParentNode()->setLeftNode(rightNode);
		else
			baseNode->getParentNode()->setRightNode(rightNode);

		rightNode->setLeftNode(baseNode);
		baseNode->recalculateHeight();
		rightNode->recalculateHeight();
		return rightNode;
	}

	Node *rotateRight(Node *baseNode)
	{
		if (baseNode == NULL)
			return NULL;

		Node *leftNode = baseNode->getLeftNode();
		baseNode->setLeftNode(leftNode->getRightNode());

		if (leftNode->getPair().first < baseNode->getParentNode()->getPair().first)
			baseNode->getParentNode()->setLeftNode(leftNode);
		else
			baseNode->getParentNode()->setRightNode(leftNode);

		leftNode->setRightNode(baseNode);
		baseNode->recalculateHeight();
		leftNode->recalculateHeight();
		return leftNode;
	}

public:
	AVLTree()
	{
		m_rootNode = NULL;
		m_slowSource = false;
		m_nodeAfterLast = new Node(ValueType());
		m_autoBalance = true;
		m_size = 0;
	}

	~AVLTree()
	{
		delete m_nodeAfterLast;
		clear();
	}

	void autoBalance(bool enable)
	{
		m_autoBalance = enable;
	}

	bool autoBalance()
	{
		return m_autoBalance;
	}

	iterator begin()
	{
		return iterator(findMinKeyNode(m_rootNode));
	}

	const iterator begin() const
	{
		return iterator(findMinKeyNode(m_rootNode));
	}

	void clear()
	{
		clear(m_rootNode);
	}

	bool empty() const
	{
		return !m_size;
	}

	iterator end()
	{
		assert(m_size > 0);

		if (m_size == 1)
			return begin();

		return iterator(m_nodeAfterLast);
	}

	const iterator end() const
	{
		assert(m_size > 0);

		if (m_size == 1)
			return begin();

		return iterator(m_nodeAfterLast);
	}

	void erase(const KeyType &key)
	{
		if (m_rootNode == NULL)
			return;

		erase(m_rootNode, key);
		fixAfterLastNode();
		m_size--;
	}

	iterator findNode(const KeyType &key)
	{
		return iterator(findNode(m_rootNode, key));
	}

	void insert(const ValueType &pair)
	{
		m_rootNode = insert(m_rootNode, pair);
		fixAfterLastNode();
		m_size++;
	}

	void makeDelay()
	{
		usleep(300000);
	}

	reverse_iterator rbegin()
	{
		assert(m_size > 0);
		return reverse_iterator(end());
	}

	const reverse_iterator rbegin() const
	{
		assert(m_size > 0);
		return reverse_iterator(end());
	}

	reverse_iterator rend()
	{
		assert(m_size > 0);
		return reverse_iterator(begin());
	}

	const reverse_iterator rend() const
	{
		assert(m_size > 0);
		return reverse_iterator(begin());
	}

	unsigned int size() const
	{
		return m_size;
	}

	void slowSource(bool slow)
	{
		m_slowSource = slow;
	}
};
