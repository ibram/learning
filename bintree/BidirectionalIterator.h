#pragma once
#include <cassert>
#include  <iterator>
#include "TreeNode.h"

template<typename T>
class BidirectionalIterator: public std::iterator<std::bidirectional_iterator_tag, T>
{
public:
	typedef typename std::iterator<std::bidirectional_iterator_tag, T>::pointer pointer;
	typedef typename std::iterator<std::bidirectional_iterator_tag, T>::reference reference;

	/*
	 * \brief Конструктор.
	 * \param ptrToObj Указатель на узел дерева.
	 */
	explicit BidirectionalIterator(AVLTreeNode<T> *ptrToNode):
		m_ptrToNode(ptrToNode)
	{

	}

	bool operator==(const BidirectionalIterator &iterator)
	{
		return m_ptrToNode == iterator.m_ptrToNode;
	}

	bool operator!=(const BidirectionalIterator &iterator)
	{
		return m_ptrToNode != iterator.m_ptrToNode;
	}

	const reference operator*() const
	{
		assert(m_ptrToNode != NULL);
		return m_ptrToNode->getPair();
	}

	const pointer operator->() const
	{
		assert(m_ptrToNode != NULL);
		return &m_ptrToNode->getPair();
	}

	BidirectionalIterator &operator++()
	{
		m_ptrToNode = increment(m_ptrToNode);
		return *this;
	}

	BidirectionalIterator operator++(int)
	{
		BidirectionalIterator oldIterator(*this);
		m_ptrToNode = increment(m_ptrToNode);
		return oldIterator;
	}

	BidirectionalIterator &operator--()
	{
		m_ptrToNode = decrement(m_ptrToNode);
		return *this;
	}

	BidirectionalIterator operator--(int)
	{
		BidirectionalIterator oldIterator(*this);
		m_ptrToNode = Decrement(m_ptrToNode);
		return oldIterator;
	}

private:
	AVLTreeNode<T> *m_ptrToNode;

	AVLTreeNode<T>* decrement(AVLTreeNode<T> *node)
	{
		if (node != NULL)
		{
			if (node->getLeftNode() != NULL && node->getLeftNode() == node->getRightNode())
				jumpToLast(node);
			else if (node->getParentNode() != NULL && node->getParentNode()->getParentNode() != NULL &&
					node->getParentNode()->getParentNode() == node)
				node = node->getRightNode();
			else if (node->getLeftNode() != NULL)
			{
				AVLTreeNode<T> *leftNode = node->getLeftNode();

				while (leftNode->getRightNode() != NULL)
					leftNode = leftNode->getRightNode();

				node = leftNode;
			}
			else
			{
				AVLTreeNode<T> *parentNode = node->getParentNode();

				while (parentNode != NULL && node == parentNode->getLeftNode())
				{
					node = parentNode;
					parentNode = parentNode->getParentNode();
				}

				node = parentNode;
			}
		}

		return node;
	}

	void jumpToLast(AVLTreeNode<T>* &baseNode)
	{
		baseNode = baseNode->getLeftNode();

		while (baseNode->getRightNode() != NULL)
			baseNode = baseNode->getRightNode();
	}

	AVLTreeNode<T>* increment(AVLTreeNode<T> *node)
	{
		if (node != NULL)
		{
			if (node->getLeftNode() != NULL && node->getLeftNode() == node->getRightNode())
				jumpToLast(node);
			else if (node->getRightNode() != NULL)
			{
				node = node->getRightNode();

				while (node->getLeftNode() != NULL)
					node = node->getLeftNode();
			}
			else //Последний узел
			{
				AVLTreeNode<T> *parentNode = node->getParentNode();

				while (parentNode != NULL && node == parentNode->getRightNode())
				{
					node = parentNode;
					parentNode = parentNode->getParentNode();
				}

				if (node->getRightNode() != parentNode && parentNode != NULL)
					node = parentNode;
			}
		}

		return node;
	}
};
