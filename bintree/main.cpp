#include <algorithm>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include "AVLTree.h"
using namespace std;

void printValue(pair<int, int> &treeValue)
{
	cout << treeValue.second << " ";
}

int main()
{
	auto_ptr<AVLTree<int, int> > tree(new AVLTree<int, int>());
	cout << "Enter numbers which should be added in container: ";
	string inputLine;
	getline(cin, inputLine, '\n');
	istringstream inputStream(inputLine);
	istream_iterator<int> inputStreamIterator(inputStream);
	istream_iterator<int> endOfStreamIterator;

	for (istream_iterator<int> i = inputStreamIterator; i != endOfStreamIterator; i++)
	{
		tree->insert(pair<int, int>(*i, *i));
		cout << "[AVLTree]: Inserted '" << *i << "'" << endl;
	}

	cout << "Sorted in descended order: ";
	for_each(tree->rbegin(), tree->rend(), printValue);
}
