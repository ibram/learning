#include <memory>

#include <gtest/gtest.h>

#include "BTree.h"

void addItems(int a, int b, int c, BTree<int, int> &tree)
{
	ASSERT_EQ(tree.size(), 0);
	ASSERT_EQ(tree.height(), 0);
	tree.insert(std::pair<int, int>(0, 0));
	tree.insert(std::pair<int, int>(1, 1));
	tree.insert(std::pair<int, int>(a, a));
	tree.insert(std::pair<int, int>(b, b));
	tree.insert(std::pair<int, int>(c, c));
	ASSERT_EQ(tree.size(), 5);
	ASSERT_EQ(tree.height(), 1);
}

//Тест вставки
TEST(BTreeTest, InsertionTest)
{
	std::auto_ptr<BTree<int, int> > tree(new BTree<int, int>(2));
	ASSERT_EQ(tree->size(), 0);

	for (int i = 0; i < 5; ++i)
		tree->insert(std::pair<int, int>(i, i));

	ASSERT_EQ(tree->size(), 5);
}

//Тест выбора родительского узла, при переполнении
TEST(BTreeTest, ParentNodeChoosing)
{
	std::auto_ptr<BTree<int, int> > tree(new BTree<int, int>(2));
	addItems(3, 2, 4, *tree);
	tree->clear();
	addItems(3, 4, 2, *tree);
}

//Тест слияния двух последних сыновей корневого узла
TEST(BTreeTest, NodeJoining)
{
	std::auto_ptr<BTree<int, int> > tree(new BTree<int, int>(2));
	tree->insert(std::pair<int, int>(0, 0));
	tree->insert(std::pair<int, int>(1, 1));
	tree->insert(std::pair<int, int>(2, 2));
	ASSERT_EQ(tree->size(), 3);
	ASSERT_EQ(tree->height(), 1);
	ASSERT_NE(tree->find(1), tree->end());
	tree->erase(1); //Слияние 0 и 2, которые будут в новом корневом узле, высота уменьшится
	ASSERT_EQ(tree->size(), 2);
	ASSERT_EQ(tree->height(), 0);
	ASSERT_EQ(tree->find(1), tree->end());
}

//Тест удаления
TEST(BTreeTest, ErasingTest)
{
	std::auto_ptr<BTree<int, int> > tree(new BTree<int, int>(2));

	for (int i = 0; i < 5; i++)
		tree->insert(std::pair<int, int>(i, i));

	ASSERT_EQ(tree->size(), 5);
	ASSERT_NE(tree->find(1), tree->end());
	tree->erase(1); //Удалили нелистовой узел
	ASSERT_EQ(tree->find(1), tree->end());
	ASSERT_EQ(tree->size(), 4);
	ASSERT_NE(tree->find(4), tree->end());
	tree->erase(4); //Удалили листовой узел
	ASSERT_EQ(tree->find(4), tree->end());
	ASSERT_EQ(tree->size(), 3);
	tree->erase(56); //Удаляем несуществующий элемент
	ASSERT_EQ(tree->size(), 3);
}

//Тест очистки
TEST(BTreeTest, ClearTest)
{
	std::auto_ptr<BTree<int, int> > tree(new BTree<int, int>(2));

	for (int i = 0; i < 5; i++)
		tree->insert(std::pair<int, int>(i, i));

	ASSERT_EQ(tree->size(), 5);
	ASSERT_NE(tree->height(), 0);
	ASSERT_FALSE(tree->empty());
	tree->clear();
	ASSERT_EQ(tree->size(), 0);
	ASSERT_EQ(tree->height(), 0);
	ASSERT_TRUE(tree->empty());
}

//Тест состояния пусто/не пусто
TEST(BTreeTest, EmptyStateTest)
{
	std::auto_ptr<BTree<int, int> > tree(new BTree<int, int>(2));
	ASSERT_TRUE(tree->empty());
	tree->insert(std::pair<int, int>(1, 1));
	ASSERT_FALSE(tree->empty());
	tree->erase(1);
	ASSERT_TRUE(tree->empty());
}

//Тест высоты дерева
TEST(BTreeTest, HeightHandlingTest)
{
	std::auto_ptr<BTree<int, int> > tree(new BTree<int, int>(2));
	ASSERT_EQ(tree->height(), 0);

	for (int i = 0; i < 3; ++i)
		tree->insert(std::pair<int, int>(1, 1));

	ASSERT_EQ(tree->height(), 1);
	tree->clear();
	ASSERT_EQ(tree->height(), 0);
}

//Тест итератора
TEST(BTreeTest, IteratorTest)
{
	BTree<int, int>::iterator it(0, 0, 0);
	std::auto_ptr<BTree<int, int> > tree(new BTree<int, int>(2));

	//Возвращаемый итератор
	it = tree->insert(std::pair<int, int>(0, 0));
	ASSERT_EQ(it->second, 0);
	it = tree->insert(std::pair<int, int>(1, 1));
	ASSERT_EQ(it->second, 1);

	for (int i = 2; i < 10; i++)
		tree->insert(std::pair<int, int>(i, i));

	tree->insert(std::pair<int, int>(2, 2));

	//Инкремент с последнего узла
	it = tree->end(); it++;
	ASSERT_NE(it, tree->end());
	ASSERT_EQ(it->second, 9);
	it--; ASSERT_EQ(it->second, 8);
	it++; ASSERT_EQ(it->second, 9);
	it++; ASSERT_EQ(it, tree->end());
	it = tree->find(1); ASSERT_EQ(it->second, 1);
	it++; ASSERT_EQ(it->second, 2); //Инкремент не с листового

	//Инкремент с листового узла
	it = tree->find(2); ASSERT_EQ(it->second, 2);
	it++; ASSERT_EQ(it->second, 2); //У нас две '2' в дереве
	it++; ASSERT_EQ(it->second, 3);

	//Декремент с последнего узла
	it = tree->end();
	it--; ASSERT_NE(it, tree->end());
	ASSERT_EQ(it->second, 9);

	//Декремент не с листового узла
	it = tree->find(1); ASSERT_EQ(it->second, 1);
	it--; ASSERT_EQ(it->second, 0); //Инкремент не с листового

	//Инкремент с листового узла
	it = tree->find(2); ASSERT_EQ(it->second, 2);
	it--; ASSERT_EQ(it->second, 1);

	//Проверка операторов == / !=
	ASSERT_FALSE(tree->begin() != tree->begin());
	ASSERT_FALSE(tree->begin() == tree->end());
}

/*Тест балансировки.
 * Случай, когда удаляем последний элемент в текущем узле, а у след. соседа
 * достаточно элементов, чтобы "передать" нам.
 */
TEST(BTreeTest, BalanceTest)
{
	std::auto_ptr<BTree<int, int> > tree(new BTree<int, int>(2));

	for (int i = 1; i < 5; i++)
		tree->insert(std::pair<int, int>(i, i));

	ASSERT_EQ(tree->size(), 4);
	ASSERT_EQ(tree->height(), 1);
	ASSERT_NE(tree->find(1), tree->end());
	tree->erase(1);
	ASSERT_EQ(tree->size(), 3);
	ASSERT_EQ(tree->height(), 1); //Высота по-прежнему та же
	ASSERT_EQ(tree->find(1), tree->end());
}
