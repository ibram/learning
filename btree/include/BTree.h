#pragma once

#include <iostream>
#include <iterator>

#include "BidirectionalIterator.h"
#include "TreeNode.h"

namespace btree
{
	/** @class BTree
	 * @brief Класс, предназначенный для работы с Б Деревом.
	 * @details Подробнее о Б Дереве читайте
	 * <a href = "http://ru.wikipedia.org/wiki/B-%D0%B4%D0%B5%D1%80%D0%B5%D0%B2%D0%BE">
	 * 		здесь
	 * </a>.
	 * @tparam T1 Тип ключа элемента.
	 * @tparam T2 Тип значения элемента.
	 * @tparam Comparer Тип компаратора.
	 */
	template<typename T1, typename T2, typename Comparer = std::less<T1> >
	class BTree
	{
	public:
		typedef T1 key_type;
		typedef T2 mapped_type;
		typedef std::pair<key_type, mapped_type> value_type;

		typedef Comparer key_compare;
		typedef TreeNode<value_type> Node;

		typedef value_type& reference;
		typedef const value_type& const_reference;
		typedef value_type* pointer;
		typedef const value_type* const_pointer;

		typedef BidirectionalIterator<value_type> iterator;
		typedef const BidirectionalIterator<value_type> const_iterator;
		typedef std::reverse_iterator<iterator> reverse_iterator;
		typedef const std::reverse_iterator<iterator> const_reverse_iterator;

		typedef ptrdiff_t difference_type;
		typedef size_t size_type;

		/** @brief Конструктор.
		 * @param t Параметр дерева. Значение не должно быть меньше 2.
		 */
		explicit BTree(size_type t = 2)
		{
			if (t < 2) m_t = 2;
			else m_t = t;
			m_size = m_height = 0;
			m_slowSource = false;
		}

		~BTree()
		{
			this->clear();
		}

		/** @brief Конструктор копирования.
		 * @param otherTree Источник, на основе которого будет создан объект.
		 */
		BTree(const BTree &otherTree)
		{
			m_t = otherTree.m_t;
			Node *copiedNode = 0;
			m_rootNode = recursiveCopy(otherTree.m_rootNode, copiedNode);
			m_afterLastNode = new Node(m_t);
			m_afterLastNode->setChildrenCount(2);
			updateAfterLastNode();
			m_size = otherTree.m_size;
			m_height = otherTree.m_height;
		}

		/** @brief Оператор присваивания.
		 * @param otherTree Источник, на основе которого будет изменен объект.
		 * @return
		 */
		BTree& operator=(const BTree &otherTree)
		{
			this->clear();
			m_t = otherTree.m_t;
			Node *copiedNode = 0;
			m_rootNode = recursiveCopy(otherTree.m_rootNode, copiedNode);
			m_afterLastNode = new Node(m_t);
			m_afterLastNode->setChildrenCount(2);
			updateAfterLastNode();
			m_size = otherTree.m_size;
			m_height = otherTree.m_height;
			return *this;
		}

		/** @brief Оператор доступа к значению по ключу.
		 * @details Если указанного ключа в дереве не существует, будет выполнена вставка нового
		 * элемента с данным ключом и значением по умолчанию.
		 * @param key Ключ, по которому функция вернет соответствующее ему значение.
		 * @return Значение, соответствующее ключу.
		 */
		mapped_type& operator[](const key_type &key)
		{
			iterator i(0, 0, 0);

			if (m_size > 0)
			{
				i = find(key);

				if (i != end())
					return i->second;
			}

			return (this->insert(value_type(key, mapped_type())))->second;
		}

		/** @brief Возвращает значение по ключу.
		 * @details Если указанного ключа в дереве не существует, будет выполнена вставка нового
		 * элемента с данным ключом и значением по умолчанию.
		 * @param key Ключ, по которому функция вернет соответствующий ему элемент.
		 * @return Значение, соответствующее ключу.
		 */
		mapped_type& at(const key_type &key)
		{
			iterator i(0, 0, 0);

			if (m_size > 0)
			{
				i = find(key);

				if (i != end())
					return i->second;
			}

			return (this->insert(value_type(key, mapped_type())))->second;
		}

		/** @brief Возвращает значение по ключу.
		 * @details Если указанного ключа в дереве не существует, будет выполнена вставка нового
		 * элемента с данным ключом и значением по умолчанию.
		 * @param key Ключ, по которому функция вернет соответствующий ему элемент.
		 * @return Значение, соответствующее ключу.
		 */
		const mapped_type& at(const key_type &key) const
		{
			iterator i(0, 0, 0);

			if (m_size > 0)
			{
				i = find(key);

				if (i != end())
					return i->second;
			}

			return (this->insert(value_type(key, mapped_type())))->second;
		}

		/** @brief Возвращает итератор на первый элемент дерева.
		 * @details Первым элементом дерева является элемент с минимальным ключем.
		 * @return Итератор на первый элемент дерева.
		 */
		iterator begin()
		{
			Node *mostLeftNode = 0;
			int index = 0;
			getMostLeftItem(m_rootNode, mostLeftNode, index);
			Node *mostRightNode = 0;
			getMostRightItem(m_rootNode, mostRightNode, index);
			return iterator(mostLeftNode, mostRightNode);
		}

		/** @brief Возвращает константный итератор на первый элемент дерева.
		 * @details Первым элементом дерева является элемент с минимальным ключем.
		 * @return Константный итератор на первый элемент дерева.
		 */
		const_iterator begin() const
		{
			Node *mostLeftNode = 0;
			int index = 0;
			getMostLeftItem(m_rootNode, mostLeftNode, index);
			Node *mostRightNode = 0;
			getMostRightItem(m_rootNode, mostRightNode, index);
			return iterator(mostLeftNode, mostRightNode);
		}

		/// @brief Очищает дерево, сбрасывая высоту и размер на 0.
		void clear()
		{
			/* Убираем один указатель, чтобы корневой узел не удалялся дважды, т.к.
			 * у служебного узла оба ребенка указывают на корневой узел.
			 */
			m_afterLastNode->setChild(1, 0);
			clear(m_afterLastNode);
			m_height = 0;
		}

		/** @brief Указывает, содержит ли дерево элементы.
		 * @return Наличие элементов в дерево. true, если дерево пустое, иначе false.
		 */
		bool empty() const
		{
			return !m_size;
		}

		/** @brief Возвращает итератор на элемент, следующий за последним элементом дерева.
		 * @details Последним элементом дерева является элемент с максимальным ключем.
		 * @return Итератор на элемент, следующий за последним элементом дерева.
		 */
		iterator end()
		{
			if (m_size < 1)
				throw std::logic_error("Size equals zero");

			int index = 0;
			Node *mostRightNode = 0;
			getMostRightItem(m_rootNode, mostRightNode, index);
			return iterator(m_afterLastNode, mostRightNode);
		}

		/** @brief Возвращает константный итератор на элемент, следующий за последним элементом дерева.
		 * @details Последним элементом дерева является элемент с максимальным ключем.
		 * @return Константный итератор на элемент, следующий за последним элементом дерева.
		 */
		const_iterator end() const
		{
			if (m_size < 1)
				throw std::logic_error("Size equals zero");

			int index = 0;
			Node *mostRightNode = 0;
			getMostRightItem(m_rootNode, mostRightNode, index);
			return iterator(m_afterLastNode, mostRightNode);
		}

		/** @brief Удаляет элемент из дерева.
		 * @param key Ключ элемента, который требуется удалить.
		 */
		void erase(const key_type &key)
		{
			if (m_size > 0)
			{
				if (m_rootNode->getChildrenCount() == 0)
				{
					m_rootNode->deleteItem(m_rootNode->getItemIndex(key));
					m_size--;
				}
				else
				{
					Node *ownerNode = 0;
					int keyIndex = 0;
					find(key, ownerNode, keyIndex);

					if (ownerNode != 0)
					{
						if (ownerNode->getChildrenCount() == 0)
							eraseFromLeaf(key, ownerNode);
						else
							eraseFromNode(key, ownerNode);
					}
				}
			}
		}

		/** @brief Возвращает итератор на элемент, содержащий указанный ключ.
		 * @details Если элемент с указанным ключем не найден, то возвращается итератор на элемент,
		 * следующий за последним элементом дерева.
		 * @param key Ключ, по которому будет произведен поиск элемента.
		 * @return Итератор на элемент, содержащий указанный ключ.
		 */
		iterator find(const key_type &key)
		{
			Node *mostRightNode = 0;
			int index = 0;
			getMostRightItem(m_rootNode, mostRightNode, index);
			Node *targetNode = 0;
			find(key, targetNode, index);

			if (targetNode != 0)
				return iterator(targetNode, mostRightNode, index);

			return end();
		}

		/** @brief Возвращает константный итератор на элемент, содержащий указанный ключ.
		 * @details Если элемент с указанным ключем не найден, то возвращается константный итератор
		 * на элемент, следующий за последним элементом дерева.
		 * @param key Ключ, по которому будет произведен поиск элемента.
		 * @return Константный итератор на элемент, содержащий указанный ключ.
		 */
		const_iterator find(const key_type &key) const
		{
			Node *mostRightNode = 0;
			int index = 0;
			getMostRightItem(m_rootNode, mostRightNode, index);
			Node *targetNode = 0;
			find(key, targetNode, index);

			if (targetNode != 0)
				return iterator(targetNode, targetNode, index);

			return end();
		}

		/** @brief Возвращает высоту дерева.
		 * @return Высота дерева.
		 */
		size_type height() const
		{
			return m_height;
		}

		/** @brief Добавляет элемент в дерево .
		 * @param item Элемент, который необходимо добавить.
		 */
		iterator insert(const_reference item)
		{
			size_type itemsIndex = 0;
			Node *usedNode = 0;

			if (m_size == 0)
			{
				m_rootNode = new Node(m_t);
				m_afterLastNode = new Node(m_t);
				m_afterLastNode->setChildrenCount(2);
				updateAfterLastNode();
			}

			insert(item, m_rootNode, usedNode, itemsIndex);

			if (m_rootNode->getItemsCount() == (2 * m_t - 1))
			{
				Node *newNode1 = 0;
				Node *newNode2 = 0;
				splitNode(m_rootNode, newNode1, newNode2);
				Node* newRootNode = new Node(m_t);
				newRootNode->addItem(m_rootNode->getItem(m_t - 1));
				newRootNode->addChild(newNode1);
				newRootNode->addChild(newNode2);

				if (usedNode == m_rootNode)
				{
					if (itemsIndex > m_t - 1)
						usedNode = newNode2;
					else if (itemsIndex < m_t - 1)
						usedNode = newNode1;
					else
						usedNode = newRootNode;
				}

				delete m_rootNode;
				m_rootNode = newRootNode;
				updateAfterLastNode();
				m_height++;
			}

			itemsIndex = usedNode->getItemIndex(item.first);
			int index = 0;
			Node *mostRightNode = 0;
			getMostRightItem(m_rootNode, mostRightNode, index);
			return iterator(usedNode, mostRightNode, itemsIndex);
		}

		/** @brief Возвращает объект, использующийся для сравнения ключей.
		 * @return Объект, использующийся для сравнения ключей.
		 */
		key_compare key_comp() const
		{
			return m_lessComparer;
		}

		/** @brief Возвращает реверсивный итератор на элемент, следующий за последним элементом дерева.
		 * @details Последним элементом дерева является элемент с максимальным ключем.
		 * @return Реверсивный итератор на элемент, следующий за последним элементом дерева.
		 */
		reverse_iterator rbegin()
		{
			if (m_size < 1)
				throw std::logic_error("Size equals zero");

			return reverse_iterator(end());
		}

		/** @brief Возвращает константный реверсивный итератор на элемент, следующий за последним
		 * элементом дерева.
		 * @details Последним элементом дерева является элемент с максимальным ключем.
		 * @return Константный реверсивный итератор на элемент, следующий за последним элементом дерева.
		 */
		const_reverse_iterator rbegin() const
		{
			if (m_size < 1)
				throw std::logic_error("Size equals zero");

			return reverse_iterator(end());
		}

		/** @brief Возвращает реверсивный итератор на первый элемент дерева.
		 * @details Первым элементом дерева является элемент с минимальным ключем.
		 * @return Реверсивный итератор на первый элемент дерева.
		 */
		reverse_iterator rend()
		{
			if (m_size < 1)
				throw std::logic_error("Size equals zero");

			return reverse_iterator(begin());
		}

		/** @brief Возвращает константный реверсивный итератор на первый элемент дерева.
		 * @details Первым элементом дерева является элемент с минимальным ключем.
		 * @return Константный реверсивный итератор на первый элемент дерева.
		 */
		const_reverse_iterator rend() const
		{
			if (m_size < 1)
				throw std::logic_error("Size equals zero");

			return reverse_iterator(begin());
		}

		/** @brief Возвращает количество элементов, находящихся в дереве.
		 * @return Количество элементов, находящихся в дереве.
		 */
		size_type size() const
		{
			return m_size;
		}

		/** @brief Устанавливает искуственное чтение с медленного носителя.
		 * @details Происходит всего-лишь имитация медленного носителя инофрмация, т.е на самом
		 * деле при каждом переходе к другому узлу во время поиска происходит пауза в 300 мс.
		 */
		void slowSource(bool slow)
		{
			m_slowSource = slow;
		}

	private:
		key_compare m_lessComparer;
		size_type m_height, m_size, m_t;
		Node *m_afterLastNode, *m_rootNode;
		bool m_slowSource;

		/** @brief Балансирует узлы при удалении листового узла.
		 * @param leafNode Ссылка на листовой узел, который был удален.
		 */
		void balance(Node *leafNode)
		{
			Node *parentNode = leafNode->getParentNode();
			Node *previousNode = leafNode->getPreviousSibling();
			Node *nextNode = leafNode->getNextSibling();
			int rSplitItemIndex = leafNode->getParentNode()->getChildIndex(leafNode);
			int lSplitItemIndex = rSplitItemIndex - 1;
			value_type lSplitItem, rSplitItem;

			if (nextNode != 0)
				rSplitItem = leafNode->getParentNode()->getItem(rSplitItemIndex);

			if (previousNode != 0)
				lSplitItem= leafNode->getParentNode()->getItem(lSplitItemIndex);

			//Если не меньше t-1, то останавливаемся.
			if (leafNode->getItemsCount() > (m_t - 2)) return;
			// Если больше t-1, то просто удаляем и больше ничего делать не нужно.
			if (nextNode != 0 && nextNode->getItemsCount() > (m_t - 1))
			{
				// Удаляем и запоминаем мин. элемент
				value_type minItem = nextNode->getItem(0);
				nextNode->deleteItem(0);
				leafNode->addChild(nextNode->getChild(0));
				nextNode->deleteChild(0, false);
				//Вместо разделителя вставляем полученный мин. элемент
				parentNode->setItem(rSplitItemIndex, minItem);
				leafNode->addItem(rSplitItem); //Добавили себе элемент-разделитель
			}
			else if (previousNode != 0 && previousNode->getItemsCount() > (m_t - 1))
			{
				// Удаляем и запоминаем макс. элемент
				value_type maxItem = previousNode->getItem(previousNode->getItemsCount() - 1);
				previousNode->deleteItem(previousNode->getItemsCount() - 1);
				leafNode->addChild(previousNode->getChild(previousNode->getChildrenCount() - 1));
				previousNode->deleteChild(previousNode->getChildrenCount() - 1, false);
				//Вместо разделителя вставляем полученный макс. элемент
				parentNode->setItem(lSplitItemIndex, maxItem);
				leafNode->addItem(lSplitItem); //Добавили себе элемент-разделитель
			}
			else if (previousNode != 0 || nextNode != 0)
			{
				leafNode = joinWithSibling(lSplitItem, rSplitItem, previousNode,
						leafNode, nextNode);

				if (parentNode->getItemsCount() == (m_t - 2) && parentNode != m_rootNode)
				{
					balance(parentNode);
					m_height--;
				}
			}

			updateAfterLastNode();
		}

		/** @brief Рекурсивно удаляет все узлы дерева.
		 * @param node Узел, с которого начинается удаление.
		 */
		void clear(Node *node)
		{
			if (node != 0 && m_size > 0)
			{
				for (size_type i = 0; i < node->getChildrenCount(); i++)
					clear(node->getChild(i));

				m_size -= node->getItemsCount();
				delete node;
			}
		}

		/** @brief Копирует потомков с узла в узел.
		 * @param startIndex Индекс, с которого начинается копирование.
		 * @param node Указатель на узел-источник.
		 * @param targetNode Указатель на узел-приемник.
		 */
		void copyChilds(size_type startIndex, Node *node, Node *&targetNode)
		{
			size_type minBound = startIndex;
			size_type maxBound = 0;

			if (minBound == 0) maxBound = m_t;
			else maxBound = node->getChildrenCount();

			if (node->getChildrenCount() > 0)
			{
				for (size_type i = minBound, j = 0; i < maxBound; i++, j++)
					targetNode->setChild(j, node->getChild(i));

				if (minBound == 0)
				{
					if (node->getChildrenCount() < m_t)
						targetNode->setChildrenCount(node->getChildrenCount());
					else
						targetNode->setChildrenCount(m_t);
				}
				else if (node->getChildrenCount() > m_t)
					targetNode->setChildrenCount(node->getChildrenCount() - m_t);
			}
		}

		/** @brief Копирует элементы с узла в узел.
		 * @param startIndex Индекс, с которого начинается копирование.
		 * @param node Указатель на узел-источник.
		 * @param targetNode Указатель на узел-приемник.
		 */
		void copyItems(size_type startIndex, Node *node, Node *&targetNode)
		{
			size_type minBound = 0;
			size_type maxBound = 0;

			if (startIndex == 0)
				maxBound = m_t - 1;
			else
			{
				minBound = m_t;
				maxBound = node->getItemsCount();
			}

			for (size_type i = minBound, j = 0; i < maxBound; i++, j++)
				targetNode->setItem(j, node->getItem(i));

			if (startIndex == 0)
			{
				if (node->getItemsCount() < m_t - 1)
					targetNode->setItemsCount(node->getItemsCount());
				else
					targetNode->setItemsCount(m_t - 1);
			}
			else if (node->getItemsCount() > m_t)
				targetNode->setItemsCount(node->getItemsCount() - m_t);
		}

		/** @brief Удаляет элемент из узла. Узел должен быть листом.
		 * @param key Ключ элемента, который необходимо удалить.
		 * @param leafNode Указатель на листовой узел, из которого будет удаляться элемент.
		 */
		void eraseFromLeaf(const key_type &key, Node *leafNode)
		{
			leafNode->deleteItem(leafNode->getItemIndex(key));
			m_size--;
			balance(leafNode);
		}

		/** @brief Удаляет элемент из внутреннего узла.
		 * @param key Ключ элемента, который необходимо удалить.
		 * @param node Внутренний узел, из которого будет удаляться ключ.
		 */
		void eraseFromNode(const key_type &key, Node *node)
		{
			int keyIndex = node->getItemIndex(key);

			if (keyIndex != -1)
			{
				Node *targetNode = 0;
				int mostRightItemIndex = 0;
				this->getMostRightItem(node->getChild(keyIndex + 1), targetNode, mostRightItemIndex);
				//Заменяем наш ключ, самым правым из его поддерева
				node->setItem(node->getItemIndex(key), targetNode->getItem(mostRightItemIndex));
				//Удаляем теперь самый правый ключ из поддерева нашего
				this->eraseFromLeaf(targetNode->getItem(mostRightItemIndex).first, targetNode);
			}
		}

		/** @brief Получает местонахождение крайнего левого элемента.
		 * @details Крайний левый элемент является элементом с минимальным ключем.
		 * @param rootNode Указатель на корневой узел, с которого начинается поиск.
		 * @param ownerNode Указатель на узел, который будет ссылаться на узел, содержащий крайний
		 * левый элемент.
		 * @param itemIndex Индекс крайнего левого элемента в узле.
		 */
		void getMostLeftItem(Node *rootNode, Node *&ownerNode, int &itemIndex)
		{
			if (rootNode != 0 && m_size > 0)
			{
				if (rootNode->getChildrenCount() > 0)
				{
					Node *currentNode = rootNode->getChild(0);

					while (currentNode->getChildrenCount() > 0)
						currentNode = currentNode->getChild(0);

					ownerNode = currentNode;
				}
				else
					ownerNode = rootNode;

				itemIndex = 0;
			}
		}

		/** @brief Получает местонахождение крайнего правого элемента.
		 * @details Крайний правый элемент является элементом с максимальным ключем.
		 * @param rootNode Указатель на корневой узел, с которого начинается поиск.
		 * @param ownerNode Указатель на узел, который будет ссылаться на узел, содержащий крайний
		 * правый элемент.
		 * @param itemIndex Индекс крайнего правого элемента в узле.
		 */
		void getMostRightItem(Node *rootNode, Node *&ownerNode, int &itemIndex)
		{
			if (rootNode != 0 && m_size > 0)
			{
				if (rootNode->getChildrenCount() > 0)
				{
					Node *currentNode = rootNode->getChild(rootNode->getItemsCount());

					while (currentNode->getChildrenCount() > 0)
						currentNode = currentNode->getChild(currentNode->getItemsCount());

					ownerNode = currentNode;
				}
				else
					ownerNode = rootNode;

				itemIndex = ownerNode->getItemsCount() - 1;
			}
		}

		/** @brief Ищет узел, с указанным ключем.
		 * @param key Ключ, по которому будет произведен поиск элемента.
		 * @param ownerNode Ссылка на узел, которая в случае успешного поиска будет указывать на
		 * найденный узел.
		 * @param keyIndex Индекс элемента, который в случае успешного поиска будет указывать индекс
		 * найденного элемента в узле.
		 */
		void find(const key_type &key, Node *&ownerNode, int &keyIndex)
		{
			if (m_size == 0) return;

			Node *node = m_rootNode;

			while (node != 0)
			{
				for (size_type i = 0; i < node->getItemsCount(); i++)
				{
					if (!m_lessComparer(node->getItem(i).first, key) &&
							!m_lessComparer(key, node->getItem(i).first))
					{
						ownerNode = node;
						keyIndex = i;
						return;
					}
				}

				int targedNodeIndex = 0;

				for(size_type i = 0; i < node->getItemsCount(); i++)
				{
					if (m_lessComparer(node->getItem(i).first,key))
						targedNodeIndex++;
				}

				node = node->getChild(targedNodeIndex);

				if (m_slowSource)
				{
					makeDelay();
				}
			}
		}

		/** @brief Добавляет элемент в дерево узла.
		 * @param item Элемент, который необходимо добавить.
		 * @param node Указатель на узел, в поддерево которого будет добавлен элемент.
		 * @param usedNode Указатель на узел, в который был добавлен элемент.
		 * @param index Индекс добавленного элемента.
		 * @return Ссылка на узел, в поддерево которого будет добавлен элемент.
		 */
		void insert(const_reference item, Node *node, Node *&usedNode, size_type &index)
		{
			if (node->getChildrenCount() == 0)
			{
				index = node->addItem(item);

				if (usedNode == 0)
					usedNode = node;

				m_size++;
			}
			else
			{
				size_type insertPosition = 0;

				for (size_type i = 0; i < node->getItemsCount(); i++)
				{
					if (item > node->getItem(i))
						insertPosition++;
					else break;
				}

				Node *targetNode = node->getChild(insertPosition);
				insert(item, targetNode, usedNode, index);

				//Если узел полон (содержит 2t - 1 элементов), разбиваем его на 2
				if (targetNode->getItemsCount() == (2 * m_t - 1))
				{
					Node *newNode1 = 0;
					Node *newNode2 = 0;
					splitNode(targetNode, newNode1, newNode2);

					if (usedNode == targetNode)
					{
						if (index > m_t - 1)
							usedNode = newNode2;
						else if (index < m_t - 1)
							usedNode = newNode1;
						else usedNode = node;
					}

					/* Указатель на targetNode в узле node замняется указателями на узлы node1
					 * и node2.*/
					node->addItem(targetNode->getItem(m_t - 1));
					node->deleteChild(insertPosition);
					node->insertChild(newNode1, insertPosition);
					node->insertChild(newNode2, insertPosition + 1);
				}

				index = usedNode->getItemIndex(item.first);
			}
		}

		/** @brief Объединяет два узла.
		 * @details Память на объединяемые узлы будет освобождена. Память выделенная для нового
		 * освобождать не требуется.
		 * @param node1 Указатель на первый узел, участвующий в объединении.
		 * @param node2 Указатель на второй узел, участвующий в объединении.
		 * @param item Элемент, который будет вставлен, при объединении.
		 * @return Указатель на результирующий узел, полученный после объединения.
		 */
		Node* joinNodes(Node *node1, Node *node2, reference item)
		{
			Node *resultNode = new Node(m_t);

			for (size_type i = 0; i < node1->getItemsCount(); i++) //Скопировали первые элементы в новый узел
				resultNode->setItem(i, node1->getItem(i));

			resultNode->setItem(node1->getItemsCount(), item);

			//Скопируем оставшиеся элементы в новый узел
			for (size_type i = 0, j = node1->getItemsCount() + 1; i < node2->getItemsCount(); i++, j++)
				resultNode->setItem(j, node2->getItem(i));

			for (size_type i = 0; i < node1->getChildrenCount(); i++) //Скопировали первых детей в новый узел
				resultNode->setChild(i, node1->getChild(i));

			//Скопируем оставшихся детей в новый узел
			for (size_type i = 0, j = node1->getChildrenCount(); i < node2->getChildrenCount(); i++, j++)
				resultNode->setChild(j, node2->getChild(i));

			//Скорректируем размеры нового узла
			resultNode->setItemsCount(node1->getItemsCount() + node2->getItemsCount() + 1);
			resultNode->setChildrenCount(node1->getChildrenCount() + node2->getChildrenCount());
			//Удаляем элемент из родительского узла
			node1->getParentNode()->deleteItem(node1->getParentNode()->getItemIndex(item.first));
			//Вместо ссылки на второй узел, ссылаемся на новый созданный
			node1->getParentNode()->setChild(node1->getParentNode()->getChildIndex(node2), resultNode);

			/* Если сливаются 2 последних потомка корня – то они становятся корнем,
			 * а предыдущий корень освобождается.
			 */
			if (m_rootNode->getChildrenCount() == 2 && node1->getParentNode() == m_rootNode &&
					node2->getParentNode() == m_rootNode)
			{
				delete m_rootNode;
				m_rootNode = resultNode;
				updateAfterLastNode();
			}

			//Удаляем старые узлы
			node1->getParentNode()->deleteChild(node1->getParentNode()->getChildIndex(node1));
			delete node2;

			return resultNode;
		}

		/** @brief Объединяет узел с одним из его соседних.
		 * @param lSplitItem Левый элемент-разделитель.
		 * @param rSplitItem Правый элемент-разделитель.
		 * @param previousNode Указатель на левого соседа.
		 * @param leafNode Указатель на исходный узел.
		 * @param nextNode Указатель на правого соседа.
		 * @return Указатель на узел, получившийся в результате объединения.
		 */
		Node* joinWithSibling(reference lSplitItem, reference rSplitItem, Node* previousNode, Node *leafNode,
				Node* nextNode)
		{
			if (previousNode != 0)
				return this->joinNodes(previousNode, leafNode, lSplitItem);
			else return this->joinNodes(leafNode, nextNode, rSplitItem);
		}

		void makeDelay()
		{
			usleep(300000);
		}

		/** @brief Рекурсивно создает копию Б-Дерева.
		 * @param node Ссылка на корневой узел дерева, с которого будет сделана копия.
		 * @param copiedNode Ссылка на корневой узел скопированного дерева.
		 * @return Скопированный узел.
		 */
		Node* recursiveCopy(Node *node, Node *&copiedNode)
		{
			if (node != 0)
			{
				Node *newNode = new Node(*node);

				for (size_type i = 0; i < node->getChildrenCount(); i++)
				{
					copiedNode = recursiveCopy(node->getChild(i), copiedNode);
					newNode->setChild(i, copiedNode);
				}

				return newNode;
			}

			return 0;
		}

		/** @brief Разбивает заданный узел на два.
		 * @details Память для новых узлов выделяется внутри функции, освобождать ее не нужно.
		 * @param node Указатель на узел, который требуется разбить.
		 * @param newNode1 Указатель на узел, в которую пойдет 1ая часть исходного узла.
		 * @param newNode2 Указатель на узел, в которую пойдет 2ая часть исходного узла.
		 */
		void splitNode(Node *node, Node *&newNode1, Node *&newNode2)
		{
			newNode1 = new Node(m_t);
			newNode2 = new Node(m_t);
			copyItems(0, node, newNode1);
			copyChilds(0, node, newNode1);
			copyItems(m_t, node, newNode2);
			copyChilds(m_t, node, newNode2);
		}

		/** @brief Обновляет узел, следующий за последним узлом дерева.
		 * @details При каждом изменении, корневого узла, требуется менять расположение служебного
		 * узла. Данный узел используется итератором для его корректной работы.
		 */
		void updateAfterLastNode()
		{
			m_afterLastNode->setChild(0, m_rootNode);
			m_afterLastNode->setChild(1, m_rootNode);
		}
	};
}
