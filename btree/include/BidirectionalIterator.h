#pragma once

#include  "TreeNode.h"

namespace btree
{
	/** @class BidirectionalIterator
	 * @brief Двунаправленный итератор для Б-Дерева.
	 * @details Данный итератор используется для последовательного обхода всех элементов дерева.
	 * @tparam T Тип элементов, которые будут посещены итератором.
	 */
	template<typename T>
	class BidirectionalIterator: public std::iterator<std::bidirectional_iterator_tag, T>
	{
	public:
		typedef typename std::iterator<std::bidirectional_iterator_tag, T>::pointer pointer;
		typedef typename std::iterator<std::bidirectional_iterator_tag, T>::reference reference;

		/** @brief Конструктор.
		 * @param ptrToNode Указатель на текущий узел.
		 * @param lastNode Указатель на последний узел дерева.
		 * @param index Индекс элемента дерева.
		 */
		explicit BidirectionalIterator(TreeNode<T> *ptrToNode, TreeNode<T> *lastNode, size_t index = 0):
				m_itemIndex(index), m_lastNode(lastNode), m_ptrToNode(ptrToNode)
		{

		}

		/** @brief Оператор равенства. Указывает, равны ли между собой два итератора.
		 * @details Итераторы считаются равными, если они указывают на один и тот же элемент дерева,
		 * т.е. если равны их указатели на узел и индексы.
		 * @param iterator Итератор, с которым идет сравнение.
		 * @return true, если итераторы равны, иначе false.
		 */
		bool operator==(const BidirectionalIterator &iterator) const
			{
			if (m_ptrToNode == iterator.m_ptrToNode &&
					m_itemIndex == iterator.m_itemIndex)
				return true;
			return false;
			}

		/** @brief Оператор неравенства. Указывает, не равны ли между собой два итератора.
		 * @details Итераторы считаются равными, если они указывают на один и тот же элемент дерева,
		 * т.е. если равны их указатели на узел и индексы.
		 * @param iterator Итератор, с которым идет сравнение.
		 * @return true, если итераторы не равны, иначе false.
		 */
		bool operator!=(const BidirectionalIterator &iterator) const
			{
			if (m_ptrToNode != iterator.m_ptrToNode ||
					m_itemIndex != iterator.m_itemIndex)
				return true;
			return false;
			}

		/** @brief Оператор разыменования указателя.
		 * @return Ссылка на текущий элемент итератора.
		 */
		const reference operator*() const
		{
			if (m_ptrToNode == 0)
				throw std::logic_error("Null internal pointer");

			return m_ptrToNode->getItem(m_itemIndex);
		}

		/** @brief Оператор обращения к члену элемента.
		 * @return Текущий элемент итератора.
		 */
		const pointer operator->() const
		{
			if (m_ptrToNode == 0)
				throw std::logic_error("Null internal pointer");

			return &m_ptrToNode->getItem(m_itemIndex);
		}

		/** @brief Оператор инкремента указателя.
		 * @return Текущий итератор.
		 */
		BidirectionalIterator& operator++()
			{
			m_ptrToNode = increment(m_ptrToNode);
			return *this;
			}

		/** @brief Оператор инкремента указателя.
		 * @return Текущий итератор.
		 */
		BidirectionalIterator operator++(int)
			{
			BidirectionalIterator oldIterator(*this);
			m_ptrToNode = increment(m_ptrToNode);
			return oldIterator;
			}

		/** @brief Оператор декремента указателя.
		 * @return Текущий итератор.
		 */
		BidirectionalIterator& operator--()
			{
			m_ptrToNode = decrement(m_ptrToNode);
			return *this;
			}

		/** @brief Оператор декремента указателя.
		 * @return Текущий итератор.
		 */
		BidirectionalIterator operator--(int)
			{
			BidirectionalIterator oldIterator(*this);
			m_ptrToNode = decrement(m_ptrToNode);
			return oldIterator;
			}

	private:
		size_t m_itemIndex;
		TreeNode<T> *m_lastNode, *m_ptrToNode;

		/** @brief Сдвигает позицию итератора на один элемент назад.
		 * @param node Указатель на текущий узел.
		 * @return Указатель на текущий узел,
		 */
		TreeNode<T>* decrement(TreeNode<T> *node)
			{
			if (node != 0)
			{
				TreeNode<T> *parentNode = node->getParentNode();

				if (node->getChildrenCount() == 2 && node->getChild(0) == node->getChild(1))
				{
					node = m_lastNode;
					m_itemIndex = node->getItemsCount() - 1;
				}
				else if (node->getChildrenCount() == 0)
				{
					if (m_itemIndex > 0)
						m_itemIndex--;
					else
					{
						while(parentNode->getChildIndex(node) == 0)
							goToNeededParent(node, parentNode);

						goToNeededParent(node, parentNode);
					}
				}
				else
				{
					parentNode = node;
					node = node->getChild(m_itemIndex);
					m_itemIndex = node->getItemsCount() - 1;

					if (m_itemIndex == node->getItemsCount() - 1)
						while (node->getChildrenCount() > 0)
							node = node->getChild(node->getItemsCount());

					m_itemIndex = node->getItemsCount() - 1;
				}
			}

			return node;

			}

		/** @brief Перемещает узел на "нужного" родителя.
		 * @details "Нужным" считается узел, в котором находится предыдущий элемент.
		 * @param node Указатель на текущий узел
		 * @param parentNode Указатель на родительский узел.
		 */
		void goToNeededParent(TreeNode<T> *&node, TreeNode<T> *&parentNode)
		{
			m_itemIndex = parentNode->getChildIndex(node) - 1;
			node = parentNode;
			parentNode = node->getParentNode();
		}

		/** @brief Сдвигает позицию итератора на один элемент вперед.
		 * @param node Указатель на текущий узел.
		 * @return Указатель на текущий узел,
		 */
		TreeNode<T>* increment(TreeNode<T> *node)
			{
			if (node != 0)
			{
				TreeNode<T> *parentNode = node->getParentNode();

				if (node->getChildrenCount() == 2 && node->getChild(0) == node->getChild(1))
				{
					node = m_lastNode;
					m_itemIndex = node->getItemsCount() - 1;
				}
				else if (node == m_lastNode && m_itemIndex == node->getItemsCount() - 1)
				{
					m_itemIndex = 0;

					while(parentNode != 0)
					{
						node = parentNode;
						parentNode = node->getParentNode();
					}
				}
				else if(node->getChildrenCount() == 0)
				{
					if (m_itemIndex != node->getItemsCount() - 1)
						m_itemIndex++;
					else
					{
						do
						{
							m_itemIndex = parentNode->getChildIndex(node);
							node = parentNode;
							parentNode = node->getParentNode();
						}
						while (m_itemIndex != 0 && m_itemIndex == node->getItemsCount());
					}
				}
				else
				{
					parentNode = node;
					node = node->getChild(m_itemIndex + 1);
					m_itemIndex = 0;

					while (node->getChildrenCount() > 0)
						node = node->getChild(0);
				}

				return node;
			}

			return 0;
			}
	};
}
