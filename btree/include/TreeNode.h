#pragma once

#include <algorithm> //std::lower_bound
#include <cstddef> //ptrdiff_t
#include <stdexcept>
#include <utility>

#include "PairsComparer.h"

namespace btree
{
	/** @class TreeNode
	 * @brief Класс, представляющий собой узел для Б-Дерева.
	 * @details Узлы в Б-Дереве хранят в себе элементы, а также указатели на дочерние узлы.
	 * @tparam T Тип элемента, хранимого в узлах.
	 */
	template<typename T, typename Comparer = PairLess<T> >
	class TreeNode
	{
	public:
		typedef typename T::first_type KeyType;
		typedef typename T::second_type ValueType;

		/** @brief Конструктор.
		 * @param t Минимальная степень дерева, в котором будет использован узел.
		 */
		explicit TreeNode(size_t t)
		{
			m_t = t;
			m_parentNode = 0;
			m_itemsCount = m_childrenCount = 0;
			m_childrenCapacity = 2 * t;
			m_itemsCapacity = m_childrenCapacity - 1;
			m_items = new T[m_itemsCapacity];
			m_children = new TreeNode*[m_childrenCapacity];
		}

		/// @brief Деструктор.
		~TreeNode()
		{
			delete[] m_items;
			delete[] m_children;
		}

		/** @brief Конструктор копирования.
		 * @param otherNode Узел, на основе которого будет создана копия.
		 */
		TreeNode(const TreeNode<T> &otherNode)
		{
			m_t = otherNode.m_t;
			m_parentNode = otherNode.m_parentNode;
			m_itemsCount = otherNode.m_itemsCount;
			m_childrenCount = otherNode.m_childrenCount;
			m_itemsCapacity = otherNode.m_itemsCapacity;
			m_childrenCapacity = otherNode.m_childrenCapacity;
			m_items = new T[m_itemsCapacity];
			m_children = new TreeNode<T>*[m_childrenCapacity];

			for (size_t i = 0; i < otherNode.m_itemsCount; i++)
				m_items[i] = otherNode.m_items[i];

			for (size_t i = 0; i < otherNode.getChildrenCount(); i++)
				m_children[i] = otherNode.m_children[i];
		}

		/** @brief Оператор присваивания.
		 * @param otherNode Узел, на основе которого будут присвоены значения.
		 * @return Ссылка на текущий узел.
		 */
		TreeNode<T>& operator=(const TreeNode<T> &otherNode)
		{
			if (this != &otherNode)
			{
				m_t = otherNode.m_t;
				m_parentNode = otherNode.m_parentNode;
				m_itemsCapacity = otherNode.m_itemsCapacity;
				m_childrenCapacity = otherNode.m_childrenCapacity;
				m_itemsCount = otherNode.m_itemsCount;
				m_childrenCount = otherNode.m_childrenCount;
				delete[] m_items;
				delete[] m_children;
				m_items = new T[m_itemsCapacity];
				m_children = new TreeNode<T>*[m_childrenCapacity];

				for (size_t i = 0; i < otherNode.m_itemsCount; i++)
					m_items[i] = otherNode.m_items[i];

				for (size_t i = 0; i < otherNode.getChildrenCount(); i++)
					m_children[i] = otherNode.m_children[i];
			}

			return *this;
		}

		/** @brief Оператор неравенства.
		 * @param otherNode Узел, с которым будет произведено сравнение.
		 * @return true, если узлы не равны, иначе false.
		 */
		bool operator!=(const TreeNode &otherNode)
			{
			for (size_t i = 0; i < otherNode.m_itemsCount; i++)
				if (m_items[i] != otherNode.m_items[i])
					return true;

			if (m_t != otherNode.m_t || m_parentNode != otherNode.m_parentNode ||
					m_itemsCapacity != otherNode.m_itemsCapacity ||
					m_childrenCapacity != otherNode.m_childrenCapacity)
				return true;

			return false;
			}

		/** @brief Добавляет ссылку на дочерний узел.
		 * @param node Ссылка на дочерний узел, которую необходимо добавить.
		 */
		void addChild(TreeNode<T> *node)
		{
			if (node != 0)
			{
				m_children[m_childrenCount] = node;
				node->setParentNode(this);
				m_childrenCount++;
			}
		}

		/** @brief Добавляет элемент в узел.
		 * @param item Элемент, который необходимо добавить.
		 * @return Индекс, по которому был добавлен элемент. При успешной вставке возвращает индекс,
		 * иначе -1.
		 */
		int addItem(const T &item)
		{
			if (m_itemsCount < m_itemsCapacity)
			{
				size_t targetIndex = 0;
				size_t i = 0;

				while (i < m_itemsCount && m_pairsComparer(m_items[i], item))
				{
					i++;
					targetIndex++;
				}

				if (m_itemsCount > 0 && targetIndex < m_itemsCount)
				{
					for (size_t i = m_itemsCount; i > targetIndex ; i--)
						m_items[i] = m_items[i - 1];
				}

				m_items[targetIndex] = item;
				m_itemsCount++;
				return targetIndex;
			}

			return -1;
		}

		/** @brief Удаляет дочерний узел и опционально освобождает память.
		 * @details Второй параметр позволяет просто убрать ссылку на узел не удаляя память, что может
		 * быть полезно при перепривязке дочернего узла узла к другому родителю.
		 * @param index Индекс дочернего узла, который необходимо удалить.
		 * @param deleteMemory Указывает, удалять ли память, выделенную под узел.
		 */
		void deleteChild(size_t index, bool deleteMemory = true)
		{
			if (index < m_childrenCount)
			{
				if (index == (m_childrenCount - 1))
				{
					if (deleteMemory)
						delete m_children[index];

					m_children[index] = 0;
				}
				else
				{
					if (deleteMemory)
						delete m_children[index];

					m_children[index] = 0;
					size_t movementBound = m_childrenCount - 1;

					for (size_t i = index; i < movementBound; i++)
						m_children[i] = m_children[i + 1];

					m_children[movementBound] = 0;
				}

				m_childrenCount--;
			}
		}

		/** @brief Удаляет элемент из узла.
		 * @param index Индекс элемента, который необходимо удалить.
		 */
		void deleteItem(size_t index)
		{
			if (index < m_itemsCount)
			{
				if (index == (m_itemsCount - 1))
					m_items[index] = T(0, 0);
				else
				{
					for (size_t i = m_itemsCount - 1; i > index; i--)
						m_items[i - 1] = m_items[i];

					m_items[m_itemsCount - 1] = T(0, 0);
				}

				m_itemsCount--;
			}
		}

		/** @brief Возвращает указатель на дочерний узел.
		 * @param index Индекс дочернего узла, который необходимо вернуть.
		 * @return При валидном индексе указатель на дочерний узел, иначе 0.
		 */
		TreeNode<T>* getChild(size_t index)
			{
			if (index < m_childrenCount)
				return m_children[index];
			return 0;
			}

		/** @brief Возвращает указатель на дочерний узел.
		 * @param index Индекс дочернего узла, который необходимо вернуть.
		 * @return При валидном индексе указатель на дочерний узел, иначе 0.
		 */
		const TreeNode<T>* getChild(size_t index) const
			{
			if (index < m_childrenCount)
				return m_children[index];
			return 0;
			}

		/** @brief Возвращает количество указателей на дочерние узлы.
		 * @return Количество указателей на дочерние узлы.
		 */
		size_t getChildrenCount() const
		{
			return m_childrenCount;
		}

		/** @brief Возвращает индекс дочернего узла.
		 * @param child Указатель на дочерний узел.
		 * @return При валидном указателе возвращает его индекс, иначе -1.
		 */
		int getChildIndex(const TreeNode *child) const
		{
			if (child != 0)
			{
				for (size_t i = 0; i < m_childrenCount; i++)
				{
					if (child == m_children[i])
						return i;
				}

				return -1;
			}

			return -1;
		}

		/** @brief Возвращает ссылку на элемент.
		 * @param index Индекс элемента, который необходимо получить.
		 * @return Ссылка на элемент. При невалидном индексе выбрасывается исключение std::logic_error
		 */
		T& getItem(size_t index)
		{
			if (m_itemsCount > 0 && index >= m_itemsCount)
				throw std::logic_error("Index out of bounds");
			return m_items[index];
		}

		/** @brief Возвращает ссылку на элемент.
		 * @param index Индекс элемента, который необходимо получить.
		 * @return Ссылка на элемент. При невалидном индексе выбрасывается исключение std::logic_error
		 */
		const T& getItem(size_t index) const
		{
			if (m_itemsCount >= index)
				throw std::logic_error("Index out of bounds");
			return m_items[index];
		}

		/** @brief Возвращает количество элементов в узле.
		 * @return Количество элементов в узле.
		 */
		size_t getItemsCount() const
		{
			return m_itemsCount;
		}

		/** @brief Возвращает индекс элемента узла.
		 * @param key Ключ элемента, индекс которого необходимо получить.
		 * @return При валидном ключе возвращает его индекс, иначе -1.
		 */
		int getItemIndex(const KeyType &key) const
		{
			if (m_itemsCount > 0)
			{
				//Заюзаем бинарный поиск
				T *ptrToPair = std::lower_bound(m_items, m_items + m_itemsCount, T(key, ValueType()),
						PairLess<T>());
				ptrdiff_t index = ptrToPair - m_items;
				return static_cast<int>(index);
			}

			return -1;
		}

		/** @brief Возвращает указатель на правый соседний узел.
		 * @details Соседним считается узел, находящийся на одной высоте с текущим и имеющий того же
		 * родителя, как и у текущего.
		 * @return Указатель на правый соседний узел
		 */
		TreeNode<T>* getNextSibling()
			{
			int nextNodeIndex = this->getParentNode()->getChildIndex(this);

			if (nextNodeIndex != -1)
				nextNodeIndex++;

			return this->getParentNode()->getChild(nextNodeIndex);
			}

		/** @brief Возвращает указатель на правый соседний узел.
		 * @details Соседним считается узел, находящийся на одной высоте с текущим и имеющий того же
		 * родителя, как и у текущего.
		 * @return Указатель на правый соседний узел
		 */
		const TreeNode<T>* getNextSibling() const
			{
			int nextNodeIndex = this->getParentNode()->getChildIndex(this);

			if (nextNodeIndex != -1)
				nextNodeIndex++;

			return this->getParentNode()->getChild(nextNodeIndex);
			}

		/** @brief Возвращает указатель на родительский узел.
		 * @return Указатель на родительский узел.
		 */
		TreeNode<T>* getParentNode()
			{
			return m_parentNode;
			}

		/** @brief Возвращает указатель на родительский узел.
		 * @return Указатель на родительский узел.
		 */
		const TreeNode<T>* getParentNode() const
			{
			return m_parentNode;
			}

		/** @brief Возвращает указатель на левый соседний узел.
		 * @details Соседним считается узел, находящийся на одной высоте с текущим и имеющий того же
		 * родителя, как и у текущего.
		 * @return Указатель на левый соседний узел
		 */
		TreeNode<T>* getPreviousSibling()
			{
			int previousNodeIndex = this->getParentNode()->getChildIndex(this);

			if (previousNodeIndex != -1)
				previousNodeIndex--;

			return this->getParentNode()->getChild(previousNodeIndex);
			}

		/** @brief Возвращает указатель на левый соседний узел.
		 * @details Соседним считается узел, находящийся на одной высоте с текущим и имеющий того же
		 * родителя, как и у текущего.
		 * @return Указатель на левый соседний узел
		 */
		const TreeNode<T>* getPreviousSibling() const
			{
			int previousNodeIndex = this->getParentNode()->getChildIndex(this);

			if (previousNodeIndex != -1)
				previousNodeIndex--;

			return this->getParentNode()->getChild(previousNodeIndex);
			}

		/** @brief Получает индексы элементов-разделителей.
		 * @param lKeyIndex В эту переменную запишется индекс левого элемента-разделителя.
		 * @param rKeyIndex В эту переменную запишется индекс правого элемента-разделителя.
		 */
		void getSplitItemsIndexes(int &lKeyIndex, int &rKeyIndex)
		{
			lKeyIndex = rKeyIndex = -1;

			if (this->getNextSibling() != 0)
				rKeyIndex = m_parentNode->getChildIndex(this);

			if (this->getPreviousSibling() != 0)
				lKeyIndex = m_parentNode->getChildIndex(this) - 1;
		}

		/** @brief Вставляет указатель на дочерний узел на указанную позицию.
		 * @param node Указатель на дочерний узел, который будет вставлен.
		 * @param index Позиция, в которую будет вставлен узел.
		 */
		void insertChild(TreeNode<T> *node, size_t index)
		{
			if (node != 0 && m_childrenCount < (2 * m_t) && index < (2 * m_t))
			{
				for (size_t i = m_childrenCount; i > index; i--)
					m_children[i] = m_children[i - 1];

				m_children[index] = node;
				node->setParentNode(this);
				m_childrenCount++;
			}
		}

		/** @brief Устанавливает новый указатель на дочерний узел по индексу.
		 * @param index Индекс, по которому будет установлен новый указатель.
		 * @param childNode Указатель на дочерний узел.
		 */
		void setChild(size_t index, TreeNode<T> *childNode)
		{
			if (childNode != 0 && index < m_childrenCapacity)
			{
				m_children[index] = childNode;
				childNode->setParentNode(this);
			}
		}

		/** @brief Устанавливает количество указателей на дочерние узлы.
		 * @param childrenCount Количество указателей на дочерние узлы.
		 */
		void setChildrenCount(size_t childrenCount)
		{
			m_childrenCount = childrenCount;
		}

		/** @brief Устанавливает новый элемент по индексу.
		 * @param index Индекс, по которому будет установлен новый элемент.
		 * @param itemValue Элемент.
		 */
		void setItem(size_t index, const T &itemValue)
		{
			if (index < m_itemsCapacity)
				m_items[index] = itemValue;
		}

		/** @brief Устанавливает количество элементов.
		 * @param itemsCount Количество элементов.
		 */
		void setItemsCount(size_t itemsCount)
		{
			m_itemsCount = itemsCount;
		}

		/** @brief Устанавливает новый родительский узел для текущего узла.
		 * @param newParentNode Новый родительский узел.
		 */
		void setParentNode(TreeNode<T> *newParentNode)
		{
			m_parentNode = newParentNode;
		}

	private:
		Comparer m_pairsComparer;
		size_t m_itemsCapacity, m_itemsCount;
		size_t m_childrenCapacity, m_childrenCount;
		size_t m_t;
		T *m_items;
		TreeNode<T> *m_parentNode;
		TreeNode<T> **m_children;
	};
}
