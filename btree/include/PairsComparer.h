#pragma once

#include <functional>

namespace btree
{
	/** @class PairLess
	 * @brief Функтор, который сравниваниет два объекта типа T.
	 * @details Данный функтор используется при бинарном поиске индекса элемента.
	 * @param T Тип объектов, которые необходимо сранвить.
	 */
	template<typename T>
	class PairLess: public std::binary_function<T, T, bool>
	{
	public:
		/** @brief Оператор вызова функции.
		 * @param obj1 Первый объект, учавствующий в сравнении.
		 * @param obj2 Второй объект, учавствующий в сравнении.
		 * @return true, если первый объект меньше второго, иначе false.
		 */
		bool operator()(const T &obj1, const T &obj2) const
		{
			return obj1.first < obj2.first;
		}
	};
}
