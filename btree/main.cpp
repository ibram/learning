#include <algorithm>
#include <iostream>
#include <memory>
#include <sstream>

#include  "BTree.h"

using namespace std;
using namespace btree;

void printValue(pair<std::string, std::string> &treeValue)
{
	cout << treeValue.second << " ";
}

int main()
{
	try
	{
		BTree<std::string, std::string>::iterator it(0, 0, 0);
		std::auto_ptr<BTree<std::string, std::string> > tree(new BTree<std::string, std::string>(2));
		tree->insert(std::pair<std::string, std::string>("abc", "abc"));
		tree->insert(std::pair<std::string, std::string>("def", "def"));
		tree->insert(std::pair<std::string, std::string>("ghi", "ghi"));
		std::cout << "tree size = " << tree->size() << std::endl;
		std::cout << "tree height = " << tree->height() << std::endl;
		std::cout << "at(\"def\") = " << tree->at("def") << std::endl;
		it = tree->find("klm");

		if (it != tree->end())
			std::cout << "\"klm\" found; \"klm\" = " << it->second << std::endl;
		else std::cout << "\"klm\" not found"<< std::endl;

		it = tree->find("abc");
		if (it != tree->end())
			std::cout << "\"abc\" found; \"abc\" = " << it->second << std::endl;
		else std::cout << "\"abc\" not found" << std::endl;

		cout << "Sorted in ascended order: "; for_each(tree->begin(), tree->end(), printValue);
		cout << endl << "Sorted in descended order: "; for_each(tree->rbegin(), tree->rend(), printValue);
	}
	catch(logic_error &treeError)
	{
		cout << endl << "Error occured (" << treeError.what() << ")";
	}

	return 0;
}
