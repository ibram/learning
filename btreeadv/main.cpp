#include <iostream>
#include <sstream>
#include <time.h>

#include <BTree.h>

#include <AVLTree.h>

using namespace btree;

int main()
{
	BTree<int, std::string> bTree(110);
	AVLTree<int, std::string> avlTree;
	bTree.slowSource(true);
	avlTree.slowSource(true);
	const int INSERT_COUNT = 2000000;
	std::cout << "Inserting " << INSERT_COUNT << " items in both trees..." << std::endl;
	timespec timeBefore, timeAfter;
	
	for (int i = 0; i < INSERT_COUNT; i++)
	{
		std::stringstream strStream;
		strStream << i;
		std::string str = strStream.str();
		bTree.insert(std::pair<int, std::string>(i, str));
		avlTree.insert(std::pair<int, std::string>(i, str));
	}

	std::cout << "Searching in both trees..." << std::endl;
	clock_gettime(CLOCK_MONOTONIC, &timeBefore);
	bTree.find(INSERT_COUNT - 1);
	clock_gettime(CLOCK_MONOTONIC, &timeAfter);
	std::cout << "[*] Searching in BTree took " << timeAfter.tv_sec - timeBefore.tv_sec +
			(timeAfter.tv_nsec - timeBefore.tv_nsec) / 1000000000.0 << " seconds." << std::endl;
	clock_gettime(CLOCK_MONOTONIC, &timeBefore);
	avlTree.findNode(INSERT_COUNT - 1);
	clock_gettime(CLOCK_MONOTONIC, &timeAfter);
	std::cout << "[*] Searching in AVLTree took " << timeAfter.tv_sec - timeBefore.tv_sec +
			(timeAfter.tv_nsec - timeBefore.tv_nsec) / 1000000000.0 << " seconds." << std::endl;

	return 0;
}
