#pragma pack(1)

#include <iostream>
#include <sstream>
#include <time.h>

#include <BTree.h>

#include <AVLTree.h>

using namespace btree;

int main()
{
	BTree<int, std::string> bTree(110);
	AVLTree<int, std::string> avlTree;
	const int INSERT_COUNT = 2000000;
	std::cout << "Inserting " << INSERT_COUNT << " items in both trees..." << std::endl;
	
	for (int i = 0; i < INSERT_COUNT; i++)
	{
		std::stringstream strStream;
		strStream << i;
		std::string str = strStream.str();
		bTree.insert(std::pair<int, std::string>(i, str));
		avlTree.insert(std::pair<int, std::string>(i, str));
	}

	size_t bTreeOcuppiedSize = sizeof(BTree<int, std::string>::Node) * INSERT_COUNT + sizeof(bTree);
	size_t avlOcuppiedSize = sizeof(AVLTree<int, std::string>::Node) * INSERT_COUNT + sizeof(avlTree);

	std::cout << "BTree occupies " << bTreeOcuppiedSize << " bytes." << std::endl;
	std::cout << "AVLTree occupies " << avlOcuppiedSize << " bytes." << std::endl;

	return 0;
}
