#pragma once
#include <cstddef>
#include "TypeTraits.h"

template <typename T> class DoubleLinkedItem
{
private:
	typedef typename TypeTraits<T>::ListType ListType;

	T m_value;
	DoubleLinkedItem *m_previousItem;
	DoubleLinkedItem *m_nextItem;

public:
	explicit DoubleLinkedItem(ListType value):
		m_value(value), m_previousItem(NULL), m_nextItem(NULL)
	{

	}

	DoubleLinkedItem<T> *GetNext() const
	{
		return m_nextItem;
	}

	DoubleLinkedItem<T> *GetPrevious() const
	{
		return m_previousItem;
	}

	T GetValue() const
	{
		return m_value;
	}

	void SetNext(DoubleLinkedItem<T> *nextItem)
	{
		m_nextItem = nextItem;
	}

	void SetPrevious(DoubleLinkedItem<T> *previousItem)
	{
		m_previousItem = previousItem;
	}
};
