#pragma once
#include <iostream>
#include "DoubleLinkedList.h"
using namespace std;

template<typename T> void PrintValues(const DoubleLinkedList<T> &list)
{
	if (list.GetLength() > 0)
	{
		DoubleLinkedItem<T> *currentItem = list.GetFirstItem();

		while (currentItem != NULL)
		{
			cout << currentItem->GetValue() << endl;
			currentItem = currentItem->GetNext();
		}
	}
}

template<typename T> void ProcessUserInput()
{
	DoubleLinkedList<T> list;
	T inputValue;

	cout << "Введите данные для сохранения в списке: ";

	while (cin >> inputValue)
		list.AddItem(inputValue, list.GetLastItem());

	if (list.GetLength() > 0)
	{
		cout << "Содержимое списка: " << endl;
		PrintValues<T>(list);
	}
	else
		cout << "Список пуст.";
}

template<> void ProcessUserInput<string>()
{
	DoubleLinkedList<string> list;
	string inputValue;

	cout << "Введите данные для сохранения в списке: ";

	while (cin >> inputValue && (inputValue != "stop" && inputValue != "стоп"))
		list.AddItem(inputValue, list.GetLastItem());

	if (list.GetLength() > 0)
	{
		cout << "Содержимое списка: " << endl;
		PrintValues<string>(list);
	}
	else
		cout << "Список пуст.";
}
