#pragma once
#include <string>

template<typename T> struct TypeTraits
{
	typedef T ListType;
};

template<> struct TypeTraits<std::string>
{
	typedef const std::string& ListType;
};
