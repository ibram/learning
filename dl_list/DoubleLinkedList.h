#pragma once
#include <cstddef>
#include  "DoubleLinkedItem.h"

template<typename T> class DoubleLinkedList
{
private:
	typedef typename TypeTraits<T>::ListType ListType;

	size_t m_length;
	DoubleLinkedItem<T> *m_firstItem;
	DoubleLinkedItem<T> *m_lastItem;

public:
	DoubleLinkedList():
		m_length(0), m_firstItem(NULL), m_lastItem(NULL)
	{

	}

	void AddItem(ListType value, DoubleLinkedItem<T> *itemBefore)
	{
		DoubleLinkedItem<T> *newItem = new DoubleLinkedItem<T>(value);
		newItem->SetPrevious(itemBefore);

		if (itemBefore != NULL)
		{
			//Если вставляем в конец, указателем на последний элемент будет новый.
			if (itemBefore->GetNext() == NULL)
				m_lastItem = newItem;

			newItem->SetNext(itemBefore->GetNext());
			itemBefore->SetNext(newItem);
		}
		else
			m_firstItem = newItem;

		if (m_length == 0)
			m_lastItem = m_firstItem;

		m_length++;
	}

	void Clear()
	{
		if (m_length > 0)
		{
			while(m_firstItem != NULL)
			{
				DoubleLinkedItem<T> *tempItem = m_firstItem->GetNext(); //Запомнили следующий
				delete m_firstItem;
				m_firstItem = tempItem;
				m_length--;
			}

			m_lastItem = m_firstItem;
		}
	}

	void DeleteItem(DoubleLinkedItem<T> *itemBefore)
	{
		if (m_length < 1)
			return;

		if (itemBefore == NULL)
		{
			DoubleLinkedItem<T> *tempItem = m_firstItem->GetNext();
			delete m_firstItem;

			if (m_firstItem->GetNext() == NULL)
				m_lastItem = m_firstItem = NULL;
			else
				m_firstItem = tempItem;
		}
		else
		{
			DoubleLinkedItem<T> *tempItem = itemBefore->GetNext()->GetNext();
			delete itemBefore->GetNext();
			itemBefore->SetNext(tempItem);

			if (tempItem != NULL)
				tempItem->SetPrevious(itemBefore);
			else
				m_lastItem = itemBefore;
		}

		m_length--;

		if (m_length == 1)
			m_lastItem = m_firstItem;
	}

	DoubleLinkedItem<T> *GetFirstItem() const
	{
		return m_firstItem;
	}

	DoubleLinkedItem<T> *GetLastItem() const
	{
		return m_lastItem;
	}

	size_t GetLength() const
	{
		return m_length;
	}
};
