#include <cstdlib>
#include <ctime>
#include <iostream>
#include <list>
#include "AdditionalFunctions.h"

int main()
{
	setlocale(LC_CTYPE, "ru_RU.utf8");
	int dataType = 0;

	DoubleLinkedList<int> myList;

	long timeBefore = clock();
	for (int i = 0; i < 10000000; i++)
		myList.AddItem(i, myList.GetLastItem());
	long timeAfter = clock();
	cout << "Time is: " << static_cast<float>((timeAfter - timeBefore)) / CLOCKS_PER_SEC << " sec" << endl;


	std::list<int> stdlist;

	long t1 = clock();
	for (int i = 0; i < 10000000; i++)
			stdlist.push_back(i);
	long t2 = clock();
	cout << "Time std::list is " << static_cast<float>((t2 - t1 )) / CLOCKS_PER_SEC << " sec" << endl << endl;

	while (dataType < 1 || dataType > 3)
	{
		system("clear");
		cout << "Выберите режим данных" << endl <<
					"1 - Целочисленный" << endl <<
					"2 - Double" << endl <<
					"3 - Строка" << endl;
		cin >> dataType;
	}

	if (dataType == 1)
		ProcessUserInput<int>();
	else if (dataType == 2)
		ProcessUserInput<double>();
	else
		ProcessUserInput<string>();

	return 0;
}
