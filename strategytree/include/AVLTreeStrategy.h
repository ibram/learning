#pragma once

#include  "IStrategy.h"

/**
 * @brief Класс, представляющи стратегию АВЛ дерева.
 * @tparam T Тип ключа.
 * @tparam X Тип значения.
 * @tparam T1 Тип объекта-сравнивателя.
 */
template<typename T, typename X, typename T1 = std::less<T> >
class AVLTreeStrategy: public IStrategy<AVLTreeNode<std::pair<T, X> >, T1>
{
public:
	typedef typename IStrategy<AVLTreeNode<std::pair<T, X> >, T1>::Node Node; ///< Тип узла.
	typedef typename Node::ValueType ValueType; ///< Тип значения.


	// IStrategy

	/**
	 * @brief Удаляет элемент из дерева по указанному ключу.
	 * @details Удаление произойдет только если ключ будет найден в дереве, в противном случае
	 * не будет выполнено никаких действий.
	 * @param node Указатель на корневой узел поддерева, в котором будет произведено удаление.
	 * @param key Ключ, по которому будет произведено удаление.
	 * @param deleted Укажет, было ли произведено удаление.
	 * @return Указатель на новый корневой узел дерева.
	 */
	Node* erase(Node *node, const T &key, bool &deleted)
	{
		deleted = false;

		if (findNode(node, key) != 0)
		{
			if (m_less(key, node->getPair().first))
				node->setLeftNode(erase(node->getLeftNode(), key, deleted));
			else if (m_less(node->getPair().first, key))
				node->setRightNode(erase(node->getRightNode(), key, deleted));
			else
			{
				Node *leftNode = node->getLeftNode();
				Node *rightNode = node->getRightNode();

				if (rightNode == 0)
					return leftNode;

				Node *minKeyNode = findMinKeyNode(rightNode);
				*node = *minKeyNode;

				if (minKeyNode->isLeftLinked())
					minKeyNode->getParentNode()->setLeftNode(0);
				else
					minKeyNode->getParentNode()->setRightNode(0);

				delete minKeyNode;
				deleted = true;
			}
		}

		return balance(node);
	}

	/**
	 * @brief Добавляет новый элемент в дерево.
	 * @param rootNode Указатель на корневой узел дерева.
	 * @param pair Значение, которое будет храниться в новом элементе.
	 * @return Указатель на новый корневой узел дерева.
	 */
	Node* insert(Node *rootNode, const ValueType &pair)
	{
		if (rootNode == 0)
			return new Node(pair);

		if (m_less(pair.first, rootNode->getPair().first))
			rootNode->setLeftNode(insert(rootNode->getLeftNode(), pair));
		else
			rootNode->setRightNode(insert(rootNode->getRightNode(), pair));

		return balance(rootNode);
	}

private:
	Node* balance(Node *node)
	{
		if (node != 0)
		{
			node->recalculateHeight();

			if (getNodeBalanceFactor(node) == 2)
			{
				if (getNodeBalanceFactor(node->getRightNode()) < 0)
					rotateRight(node->getRightNode());

				return rotateLeft(node);
			}

			if (getNodeBalanceFactor(node) == -2)
			{
				if (getNodeBalanceFactor(node->getLeftNode()) > 0)
					rotateLeft(node->getLeftNode());

				return rotateRight(node);
			}
		}
		return node;
	}

	Node* removeMinKeyNode(Node *minKeyNode)
	{
	    if(minKeyNode->getLeftNode() == 0)
	        return minKeyNode->getRightNode();

	    minKeyNode->setLeftNode(removeMinKeyNode(minKeyNode->getLeftNode()));
	    return balance(minKeyNode);
	}

	Node* rotateLeft(Node *baseNode)
	{
		if (baseNode == 0)
			return 0;

		Node *rightNode = baseNode->getRightNode();
		baseNode->setRightNode(rightNode->getLeftNode());

		if (m_less(rightNode->getPair().first, baseNode->getParentNode()->getPair().first))
			baseNode->getParentNode()->setLeftNode(rightNode);
		else
			baseNode->getParentNode()->setRightNode(rightNode);

		rightNode->setLeftNode(baseNode);
		baseNode->recalculateHeight();
		rightNode->recalculateHeight();
		return rightNode;
	}

	Node* rotateRight(Node *baseNode)
	{
		if (baseNode == 0)
			return 0;

		Node *leftNode = baseNode->getLeftNode();
		baseNode->setLeftNode(leftNode->getRightNode());

		if (leftNode->getPair().first < baseNode->getParentNode()->getPair().first)
			baseNode->getParentNode()->setLeftNode(leftNode);
		else
			baseNode->getParentNode()->setRightNode(leftNode);

		leftNode->setRightNode(baseNode);
		baseNode->recalculateHeight();
		leftNode->recalculateHeight();
		return leftNode;
	}
};
