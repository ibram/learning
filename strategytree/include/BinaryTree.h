#pragma once

#include <functional>

#include "BidirectionalIterator.h"
#include "IStrategy.h"

/**
 * @brief Класс двоичного дерева.
 * @details Данное двоичное дерево поддерживает изменение поведения за счет использования
 * стратегий.
 * @tparam T Тип ключа
 * @tparam X Тип значения.
 * @tparam S Тип стратегии.
 * @tparam T4 Тип объекта-сравнивателя для ключей.
 */
template <typename T, typename X, template<typename T1, typename T2, typename T3> class S,
	typename T4 = std::less<T> >
class BinaryTree
{
public:
	typedef S<T, X, T4> Strategy; ///< Тип стратегии.
	typedef typename Strategy::Node Node; ///< Тип узла.
	typedef typename Node::KeyType KeyType; ///< Тип ключа.
	typedef typename Node::ValueType ValueType; ///< Тип значения.
	typedef BidirectionalIterator<ValueType, Strategy> iterator; ///< Тип итератора.
	typedef std::reverse_iterator<iterator> reverse_iterator; ///< Тип реверсивного итератора.

	/**
	 * @brief Конструктор.
	 */
	BinaryTree()
	{
		m_rootNode = 0;
		m_nodeAfterLast = new Node(ValueType());
		m_size = 0;
	}

	/**
	 * @brief Деструктор.
	 */
	~BinaryTree()
	{
		delete m_nodeAfterLast;
		clear();
	}

	/**
	 * @brief Возвращает итератор, указывающий на первый элемент.
	 * @return Итератор, указывающий на первый элемент.
	 */
	iterator begin()
	{
		return iterator(m_usedStrategy.findMinKeyNode(m_rootNode));
	}

	/**
	 * @brief Возвращает итератор, указывающий на первый элемент.
	 * @return Итератор, указывающий на первый элемент.
	 */
	const iterator begin() const
	{
		return iterator(m_usedStrategy.findMinKeyNode(m_rootNode));
	}

	/**
	 * @brief Выполняет очистку дерева.
	 * @details Под очисткой дерева понимается удаление всех его элементов.
	 */
	void clear()
	{
		clear(m_rootNode);
	}

	/**
	 * @brief Указывает, является ли дерево пустым.
	 * @details Дерево считается пустым, если в нем не содержится ни одного элемента.
	 * @return true, если дерево пусто, иначе false.
	 */
	bool empty() const
	{
		return !m_size;
	}

	/**
	 * @brief Возвращает итератор, указывающий на служебный элемент, следующий за последним.
	 * @return Итератор, указывающий на служебный элемент следующий за последним.
	 */
	iterator end()
	{
		assert(m_size > 0);

		if (m_size == 1)
			return begin();

		return iterator(m_nodeAfterLast);
	}

	/**
	 * @brief Возвращает итератор, указывающий на служебный элемент, следующий за последним.
	 * @return Итератор, указывающий на служебный элемент следующий за последним.
	 */
	const iterator end() const
	{
		assert(m_size > 0);

		if (m_size == 1)
			return begin();

		return iterator(m_nodeAfterLast);
	}

	/**
	 * @brief Удаляет элемент из дерева.
	 * @details Элемент будет удален, если такой существует, в противном случае ничего не произойдет.
	 * @param key Ключ, по которому будет иеднтифицирован элемент для удаления.
	 */
	void erase(const KeyType &key)
	{
		if (m_rootNode == 0)
			return;

		bool deleted = false;
		m_usedStrategy.erase(m_rootNode, key, deleted);

		// Проверим, был ли элемент удален или же такого элемента нет в дереве.
		if (deleted)
		{
			fixAfterLastNode();
			m_size--;
		}
	}

	/**
	 * @brief Находит элемент по указанному ключу.
	 * @param key Ключ, по которому будет произведен поиск элемента.
	 * @return В случае успешного поиска итератор, указывающий на найденный элемент, иначе
	 * итератор, указывающий на элемент, следующий за последним.
	 */
	iterator find(const KeyType &key)
	{
		Node *foundNode = m_usedStrategy.findNode(m_rootNode, key);

		if (foundNode != 0)
			return iterator(foundNode);

		return end();
	}

	/**
	 * @brief Добавляет новый элемент в дерево.
	 * @param pair Значение, которое необходимо добавить.
	 */
	void insert(const ValueType &pair)
	{
		m_rootNode = m_usedStrategy.insert(m_rootNode, pair);
		fixAfterLastNode();
		m_size++;
	}

	/**
	 * @brief Возвращает итератор, указывающий на служебный элемент, следующий за последним.
	 * @return Итератор, указывающий на служебный элемент следующий за последним.
	 */
	reverse_iterator rbegin()
	{
		assert(m_size > 0);
		return reverse_iterator(end());
	}

	/**
	* @brief Возвращает итератор, указывающий на служебный элемент, следующий за последним.
	* @return Итератор, указывающий на служебный элемент следующий за последним.
	*/
	const reverse_iterator rbegin() const
	{
		assert(m_size > 0);
		return reverse_iterator(end());
	}

	/**
	 * @brief Возвращает реверсивный итератор, указывающий на первый элемент.
	 * @return Реверсивный итератор, указывающий на первый элемент.
	 */
	reverse_iterator rend()
	{
		assert(m_size > 0);
		return reverse_iterator(begin());
	}

	/**
	 * @brief Возвращает реверсивный итератор, указывающий на первый элемент.
	 * @return Реверсивный итератор, указывающий на первый элемент.
	 */
	const reverse_iterator rend() const
	{
		assert(m_size > 0);
		return reverse_iterator(begin());
	}

	/**
	 * @brief Возвращает количество элементов хранящихся в дереве.
	 * @return Количество элементов, хранящихся в дереве.
	 */
	unsigned int size() const
	{
		return m_size;
	}

private:
	Strategy m_usedStrategy;
	Node *m_rootNode, *m_nodeAfterLast;
	unsigned int m_size;

	void clear(Node *rootNode)
	{
		if (rootNode != 0)
		{
			if (rootNode->getRightNode() != 0)
				clear(rootNode->getRightNode());
			if (rootNode->getLeftNode() != 0)
				clear(rootNode->getLeftNode());

			delete rootNode;
		}
	}

	void fixAfterLastNode()
	{
		m_nodeAfterLast->setLeftNode(m_rootNode);
		m_nodeAfterLast->setRightNode(m_rootNode);
	}

	/**
	 *  @brief Возвращает баланс узла.
	 * Баланс узла - разница между высотой правого поддерева и левого поддерева.
	 * @return Баланс узла.
	 */
	int getNodeBalanceFactor(const Node *node) const
	{
		if (node != 0)
			return getNodeHeigth(node->getRightNode()) - getNodeHeigth(node->getLeftNode());

		return 0;
	}

	/**
	 * @brief Возвращает высоту узла.
	 * @return Высота узла.
	 */
	unsigned char getNodeHeigth(const Node *node) const
	{
		if (node != 0)
			return node->getHeight();

		return 0;
	}

	Node *getMinKeyNode(Node *node)
	{
		if (node == 0)
			return 0;

		if (node->getLeftNode() == 0)
			return node->getRightNode();

		node->setLeftNode(getMinKeyNode(node->getLeftNode()));
		return m_usedStrategy.balance(node);
	}

	Node *rotateLeft(Node *baseNode)
	{
		if (baseNode == 0)
			return 0;

		Node *rightNode = baseNode->getRightNode();
		baseNode->setRightNode(rightNode->getLeftNode());

		if (rightNode->getPair().first < baseNode->getParentNode()->getPair().first)
			baseNode->getParentNode()->setLeftNode(rightNode);
		else
			baseNode->getParentNode()->setRightNode(rightNode);

		rightNode->setLeftNode(baseNode);
		baseNode->recalculateHeight();
		rightNode->recalculateHeight();
		return rightNode;
	}

	Node *rotateRight(Node *baseNode)
	{
		if (baseNode == 0)
			return 0;

		Node *leftNode = baseNode->getLeftNode();
		baseNode->setLeftNode(leftNode->getRightNode());

		if (leftNode->getPair().first < baseNode->getParentNode()->getPair().first)
			baseNode->getParentNode()->setLeftNode(leftNode);
		else
			baseNode->getParentNode()->setRightNode(leftNode);

		leftNode->setRightNode(baseNode);
		baseNode->recalculateHeight();
		leftNode->recalculateHeight();
		return leftNode;
	}
};
