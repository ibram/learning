#pragma once

#include "BinaryTreeNode.h"

/**
 * @brief Класс, представляющий собой узел красно-черного дерева.
 * @tparam T Тип данных, которые хранятся в узле.
 */
template<typename T>
class RBTreeNode: public BinaryTreeNode<T, RBTreeNode<T> >
{
public:
	typedef typename BinaryTreeNode<T, RBTreeNode<T> >::ValueType ValueType; ///< Тип значения.
	typedef typename BinaryTreeNode<T, RBTreeNode<T> >::KeyType KeyType; ///< Тип ключа.

	/**
	 * @brief Конструктор.
	 * @param key Ключ узла.
	 * @param value Значение узла.
	 */
	explicit RBTreeNode(const ValueType &pair): BinaryTreeNode<T, RBTreeNode<T> >(pair)
	{
		m_isBlack = false;
	}

	/**
	 * @brief Копирующий конструктор.
	 * @param node Узел-источник, с которого будет произведена копия.
	 */
	RBTreeNode(const RBTreeNode<T> &node): BinaryTreeNode<T, RBTreeNode<T> >(node)
	{
		m_isBlack = node.m_isBlack;
	}

	/**
	 * @brief Оператор присваивания.
	 * @param node Узел-источник, на основе которого будет произведено присваивание.
	 * @return Ссылка на текущий узел.
	 */
	RBTreeNode& operator=(const RBTreeNode<T> &node)
	{
		if (*this != node)
		{
			BinaryTreeNode<T, RBTreeNode<T> >::operator=(node);
			m_isBlack = node.m_isBlack;
		}

		return *this;
	}

	/**
	 * @brief Оператор равенства.
	 * @details Узлы считаются равными, если равных их ключи, значения, высоты и цвета.
	 * @param node Узел, с которым необходимо произвести сравнение.
	 * @return true, если узлы равны, иначе false.
	 */
	bool operator==(const RBTreeNode<T> &node)
	{
		return BinaryTreeNode<T, RBTreeNode<T> >::operator==(node) &&
				m_isBlack == node.m_isBlack;
	}

	/**
	 * @brief Оператор неравенства.
	 * @details Узлы считаются равными, если равных их ключи, значения, высоты и цвета.
	 * @param node Узел, с которым необходимо произвести сравнение.
	 * @return true, если узлы не равны, иначе false.
	 */
	bool operator!=(const RBTreeNode<T> &node)
	{
		return !(*this == node);
	}

	/**
	 * @brief Указывает, является ли узел черным.
	 * @return true, если узел черный, иначе false.
	 */
	bool isBlack() const
	{
		return m_isBlack;
	}

	/**
	 * @brief Указывает, является ли узел красным.
	 * @return true, если узел красный, иначе false.
	 */
	bool isRed() const
	{
		return !m_isBlack;
	}

	/**
	 * @brief Окрашивает узел в черный цвет.
	 */
	void setBlack()
	{
		m_isBlack = true;
	}

	/**
	 * @brief Окрашивает узел в красный цвет.
	 */
	void setRed()
	{
		m_isBlack = false;
	}

private:
	bool m_isBlack;
};
