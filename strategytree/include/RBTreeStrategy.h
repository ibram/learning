#pragma once

#include "IStrategy.h"
#include "RBTreeNode.h"

/**
 * @brief Класс, представляющи стратегию красно-черного дерева.
 * @tparam T Тип ключа.
 * @tparam X Тип значения.
 * @tparam T1 Тип объекта-сравнивателя.
 */
template<typename T, typename X, typename T1 = std::less<T> >
class RBTreeStrategy: public IStrategy<RBTreeNode<std::pair<T, X> >, T1>
{
public:
	typedef typename IStrategy<RBTreeNode<std::pair<T, X> >, T1>::Node Node; ///< Тип узла

	// IStrategy

	/**
	 * @brief Удаляет элемент из дерева по указанному ключу.
	 * @details Удаление произойдет только если ключ будет найден в дереве, в противном случае
	 * не будет выполнено никаких действий.
	 * @param node Указатель на корневой узел поддерева, в котором будет произведено удаление.
	 * @param key Ключ, по которому будет произведено удаление.
	 * @param deleted Укажет, было ли произведено удаление.
	 * @return Указатель на новый корневой узел дерева.
	 */
	Node* erase(Node *node, const T &key, bool &deleted)
	{
		Node *foundNode = findNode(node, key);
		deleted = false;

		if (foundNode != 0)
		{
			if (foundNode->getChildrenCount() == 0) // 1. Если у узла нет детей.
			{
				if (foundNode->getParentNode() != 0)
				{
					if (foundNode->isLeftLinked())
						foundNode->getParentNode()->setLeftNode(0);
					else
						foundNode->getParentNode()->setRightNode(0);
				}

				delete foundNode;
				deleted = true;
				return node;
			}

			if (foundNode->getChildrenCount() == 1) // 2. Если у узла 1 ребенок
			{
				Node *childrenToRelink = foundNode->getLeftNode();

				if (foundNode->getRightNode() != 0)
					childrenToRelink = foundNode->getRightNode();

				if (foundNode->isRightLinked())
					foundNode->getParentNode()->setRightNode(childrenToRelink);
				else if (foundNode->isLeftLinked())
					foundNode->getParentNode()->setLeftNode(childrenToRelink);

				delete foundNode;
				deleted = true;
				return node;
			}

			if (foundNode->getChildrenCount() == 2) // 3. Если у узла есть 2 ребенка
			{
				// Найдем узел с максимальным ключем в левом поддереве
				Node *nextMaxNode = foundNode->getLeftNode();

				while (nextMaxNode->getRightNode() != 0)
					nextMaxNode = nextMaxNode->getRightNode();

				*foundNode = *nextMaxNode;

				if (nextMaxNode->isLeftLinked())
					nextMaxNode->getParentNode()->setLeftNode(0);
				else
					nextMaxNode->getParentNode()->setRightNode(0);

				delete nextMaxNode;
				deleted = true;
				return balance(foundNode);
			}
		}

		return node;
	}

	/**
	 * @brief Добавляет новый элемент в дерево.
	 * @param rootNode Указатель на корневой узел дерева.
	 * @param pair Значение, которое будет храниться в новом элементе.
	 * @return Указатель на новый корневой узел дерева.
	 */
	Node* insert(Node *rootNode, const std::pair<T, X> &pair)
	{
		Node* newNode = insertNewNode(rootNode, pair);
		Node *newRootNode = balance(newNode);

		if (!isRoot(newRootNode))
			return rootNode;

		return newRootNode;
	}

private:
	Node* balance(Node* node)
	{
		// Рассматриваем 5 случаев восстановления состояния дерева.
		if (isRoot(node)) // 1. Узел является корневым.
		{
			node->setBlack();
			return node;
		}

		if (node->getParentNode()->isBlack()) // 2. Родитель нового узла - черный.
			return node;

		// 3. Родитель и дядя красные.
		Node* uncleNode = getNodesUncle(node);

		if (uncleNode != 0 && uncleNode->isRed() &&
				node->getParentNode()->isRed())
		{
			repaintNodes(node, uncleNode);
			return balance(node->getParentNode()->getParentNode());
		}

		/* 4. Родитель красный, дядя черный, узел - левый/правый потомок родителя, а родитель
		 * правый/левый потомок деда. */
		if (node->getParentNode()->isRed() && (uncleNode == 0 || uncleNode->isBlack()))
		{
			if (node->isRightLinked() && node->getParentNode()->isLeftLinked())
			{
				rotateLeft(node);
				return rotateRight(node);
			}

			if (node->isLeftLinked() && node->getParentNode()->isRightLinked())
			{
				rotateRight(node);
				return rotateLeft(node);
			}

			/* 5. Родитель красный, дядя черный, узел - левый/правый потомок родителя, а родитель
			 * левый/правый потомок деда.*/

			if (node->isLeftLinked() && node->getParentNode()->isLeftLinked())
			{
				node->getParentNode()->setBlack();
				node->getParentNode()->getParentNode()->setRed();
				Node *newRoot = rotateRight(node->getParentNode());
				newRoot->recalculateHeight();
				return newRoot;
			}

			if (node->isRightLinked() && node->getParentNode()->isRightLinked())
			{
				node->getParentNode()->setBlack();
				node->getParentNode()->getParentNode()->setRed();
				Node *newRoot = rotateLeft(node->getParentNode());
				newRoot->recalculateHeight();
				return newRoot;
			}
		}

		return node;
	}

	Node* getNodesUncle(Node *node)
	{
		if (node->getParentNode() == node->getParentNode()->getParentNode()->getLeftNode())
			return node->getParentNode()->getParentNode()->getRightNode();
		else
			return node->getParentNode()->getParentNode()->getLeftNode();
	}

	Node* insertNewNode(Node* rootNode, const std::pair<T, X>& pair)
	{
		if (rootNode != 0)
		{
			Node *newNode = 0;

			if (m_less(pair.first, rootNode->getPair().first))
			{
				if (rootNode->getLeftNode() == 0)
				{
					newNode = new Node(pair);
					rootNode->setLeftNode(newNode);
				}
				else
					newNode = insertNewNode(rootNode->getLeftNode(), pair);
			}
			else
			{
				if (rootNode->getRightNode() == 0)
				{
					newNode = new Node(pair);
					rootNode->setRightNode(newNode);
				}
				else
					newNode = insertNewNode(rootNode->getRightNode(), pair);
			}

			rootNode->recalculateHeight();
			return newNode;
		}

		Node *node = new Node(pair);
		node->recalculateHeight();
		return node;
	}

	bool isRoot(const Node *node) const
	{
		if (node != 0)
		{
			if (node->getParentNode() == 0)
				return true;
			else
				return node == node->getParentNode()->getLeftNode() &&
						node == node->getParentNode()->getRightNode();
		}

		return false;
	}

	void repaintNodes(Node* node, Node* uncleNode)
	{
		uncleNode->setBlack();
		node->getParentNode()->setBlack();
		node->getParentNode()->getParentNode()->setRed();
	}

	Node* rotateLeft(Node *parentNode)
	{
		Node *grandParentNode = parentNode->getParentNode();
		grandParentNode->setRightNode(parentNode->getLeftNode());

		if (grandParentNode->getParentNode() != 0)
		{
			if (!isRoot(grandParentNode))
			{
				if (grandParentNode->isLeftLinked())
					grandParentNode->getParentNode()->setLeftNode(parentNode);
				else
					grandParentNode->getParentNode()->setRightNode(parentNode);

				parentNode->setLeftNode(grandParentNode);
			}
			else // Делаем родительский узел корнем, привязывая его к служебному узлу.
			{
				grandParentNode->getParentNode()->setLeftNode(parentNode);
				grandParentNode->getParentNode()->setRightNode(parentNode);
			}
		}

		parentNode->setLeftNode(grandParentNode);
		grandParentNode->recalculateHeight();
		parentNode->recalculateHeight();
		return parentNode;
	}

	Node* rotateRight(Node *parentNode)
	{
		Node *grandParentNode = parentNode->getParentNode();
		grandParentNode->setLeftNode(parentNode->getRightNode());

		if (grandParentNode->getParentNode() != 0)
		{
			if (!isRoot(grandParentNode))
			{
				if (grandParentNode->isLeftLinked())
					grandParentNode->getParentNode()->setLeftNode(parentNode);
				else
					grandParentNode->getParentNode()->setRightNode(parentNode);

				parentNode->setRightNode(grandParentNode);
			}
			else // Делаем родительский узел корнем, привязывая его к служебному узлу.
			{
				grandParentNode->getParentNode()->setLeftNode(parentNode);
				grandParentNode->getParentNode()->setRightNode(parentNode);
			}
		}

		parentNode->setRightNode(grandParentNode);
		parentNode->recalculateHeight();
		grandParentNode->recalculateHeight();
		return parentNode;
	}
};
