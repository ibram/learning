#pragma once

#include <cstddef>
#include <utility>

#include "BinaryTreeNode.h"

/**
 * @brief Класс, представляющий собой узел АВЛ дерева.
 * @tparam T Тип данных, которые хранятся в узле.
 */
template<typename T>
class AVLTreeNode: public BinaryTreeNode<T, AVLTreeNode<T> >
{
public:
	typedef typename BinaryTreeNode<T, AVLTreeNode<T> >::ValueType ValueType; ///< Тип значения.
	typedef typename BinaryTreeNode<T, AVLTreeNode<T> >::KeyType KeyType; ///< Тип ключа.

	/**
	 * @brief Конструктор.
	 * @param key Ключ узла.
	 * @param value Значение узла.
	 */
	explicit AVLTreeNode(const ValueType &pair): BinaryTreeNode<T, AVLTreeNode<T> >(pair)
	{

	}
};
