#pragma once
#include <cassert>
#include  <iterator>

/**
 * @brief Класс, представляющий двунаправленный итератор для двоичного дерева.
 * @tparam T Тип значения.
 * @tparam X Тип стратегии.
 */
template<typename T, typename X>
class BidirectionalIterator: public std::iterator<std::bidirectional_iterator_tag, T>
{
public:
	typedef typename X::Node Node; ///< Тип узла.
	typedef typename std::iterator<std::bidirectional_iterator_tag, T>::pointer pointer; ///< Тип указателя на узел.
	typedef typename std::iterator<std::bidirectional_iterator_tag, T>::reference reference; ///< Тип ссылки на узел.

	/**
	 * @brief Конструктор.
	 * @param ptrToNode Указатель на узел дерева.
	 */
	explicit BidirectionalIterator(Node *ptrToNode):
		m_ptrToNode(ptrToNode)
	{

	}

	/**
	 * @brief Оператор равенства.
	 * @param iterator Объект, с которым необходимо произвести сравнение.
	 * @return true, если объекты равны, иначе false.
	 */
	bool operator==(const BidirectionalIterator &iterator)
	{
		return m_ptrToNode == iterator.m_ptrToNode;
	}

	/**
	 * @brief Оператор неравенства.
	 * @param iterator Объект, с которым необходимо произвести сравнение.
	 * @return true, если объекты не равны, иначе false.
	 */
	bool operator!=(const BidirectionalIterator &iterator)
	{
		return m_ptrToNode != iterator.m_ptrToNode;
	}

	/**
	 * @brief Оператор разыменования итератора.
	 * @return Константная ссылка на значение текущего узла.
	 */
	const reference operator*() const
	{
		assert(m_ptrToNode != 0);
		return m_ptrToNode->getPair();
	}

	/**
	 * @brief Оператор обращения к члену.
	 * @return Константый указатель на значение текущего узла.
	 */
	const pointer operator->() const
	{
		assert(m_ptrToNode != 0);
		return &m_ptrToNode->getPair();
	}

	/**
	 * @brief Оператор префиксного инкремента.
	 * @return Текущий объект.
	 */
	BidirectionalIterator &operator++()
	{
		m_ptrToNode = increment(m_ptrToNode);
		return *this;
	}

	/**
	 * @brief Оператор постфиксного инкремента.
	 * @return Текущий объект.
	 */
	BidirectionalIterator operator++(int)
	{
		BidirectionalIterator oldIterator(*this);
		m_ptrToNode = increment(m_ptrToNode);
		return oldIterator;
	}

	/**
	 * @brief Оператор префиксного декремента.
	 * @return Текущий объект.
	 */
	BidirectionalIterator &operator--()
	{
		m_ptrToNode = decrement(m_ptrToNode);
		return *this;
	}

	/**
	 * @brief Оператор постфиксного декремента.
	 * @return Текущий объект.
	 */
	BidirectionalIterator operator--(int)
	{
		BidirectionalIterator oldIterator(*this);
		m_ptrToNode = Decrement(m_ptrToNode);
		return oldIterator;
	}

private:
	Node *m_ptrToNode;

	Node* decrement(Node *node)
	{
		if (node != 0)
		{
			if (node->getLeftNode() != 0 && node->getLeftNode() == node->getRightNode())
				jumpToLast(node);
			else if (node->getParentNode() != 0 && node->getParentNode()->getParentNode() != 0 &&
					node->getParentNode()->getParentNode() == node)
				node = node->getRightNode();
			else if (node->getLeftNode() != 0)
			{
				Node *leftNode = node->getLeftNode();

				while (leftNode->getRightNode() != 0)
					leftNode = leftNode->getRightNode();

				node = leftNode;
			}
			else
			{
				Node *parentNode = node->getParentNode();

				while (parentNode != 0 && node == parentNode->getLeftNode())
				{
					node = parentNode;
					parentNode = parentNode->getParentNode();
				}

				node = parentNode;
			}
		}

		return node;
	}

	void jumpToLast(Node* &baseNode)
	{
		baseNode = baseNode->getLeftNode();

		while (baseNode->getRightNode() != 0)
			baseNode = baseNode->getRightNode();
	}

	Node* increment(Node *node)
	{
		if (node != 0)
		{
			if (node->getLeftNode() != 0 && node->getLeftNode() == node->getRightNode())
				jumpToLast(node);
			else if (node->getRightNode() != 0)
			{
				node = node->getRightNode();

				while (node->getLeftNode() != 0)
					node = node->getLeftNode();
			}
			else //Последний узел
			{
				Node *parentNode = node->getParentNode();

				while (parentNode != 0 && node == parentNode->getRightNode())
				{
					node = parentNode;
					parentNode = parentNode->getParentNode();
				}

				if (node->getRightNode() != parentNode && parentNode != 0)
					node = parentNode;
			}
		}

		return node;
	}
};
