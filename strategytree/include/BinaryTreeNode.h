#pragma once

/**
 * @brief Класс, представляющий собой абстрактный узел.
 * @tparam T Тип значения.
 * @tparam X Тип узла, который будет использоваться внутри.
 */
template<typename T, typename X>
class BinaryTreeNode
{
public:
	typedef T ValueType; ///< Тип значения.
	typedef typename ValueType::first_type KeyType; ///< Тип ключа.

	/**
	 * @brief Конструктор.
	 * @param pair Значение, которое будет храниться в узле.
	 */
	explicit BinaryTreeNode(const ValueType &pair): m_pair(pair)
	{
		m_parentNode = m_leftNode = m_rightNode = 0;
		m_height = 1;
	}

	/**
	 * @brief Копирующий конструктор.
	 * @details Копируются только высота и значение. Ссылки на соседние узлы указывают на 0.
	 * @param node Узел, с которого будет произведено копирование.
	 */
	BinaryTreeNode(const BinaryTreeNode &node)
	{
		m_height = node.m_height;
		m_pair = node.m_pair;
		m_parentNode = m_leftNode = m_rightNode = 0;
	}

	/**
	 * @brief Деструктор.
	 */
	virtual ~BinaryTreeNode()
	{

	}

	/**
	 * @brief Оператор присваивания.
	 * @details Будут присвоены только высота и значение узла. Ссылки на другие узлы останутся без
	 * изменений.
	 * @param node Узел, на основе которого будет произведено присваивание.
	 * @return Текущий объект.
	 */
	BinaryTreeNode& operator=(const BinaryTreeNode &node)
	{
		if (*this != node)
		{
			m_height = node.m_height;
			m_pair = node.m_pair;
		}

		return *this;
	}

	/**
	 * @brief Оператор равенства.
	 * @details Узлы считаются равными, если равны их значения и высоты. Ссылки на другие узлы не
	 * учитываются.
	 * @param node Узел, с которым необходимо произвести сравнение.
	 * @return true, если узлы равны, иначе false.
	 */
	bool operator==(const BinaryTreeNode &node)
	{
		return (m_pair == node.m_pair) && (m_height == node.m_height);
	}

	/**
	 * @brief Оператор неравенства.
	 * @details Узлы считаются равными, если равны их значения и высоты. Ссылки на другие узлы не
	 * учитываются.
	 * @param node Узел, с которым необходимо произвести сравнение.
	 * @return true, если узлы не равны, иначе false.
	 */
	bool operator!=(const BinaryTreeNode &node)
	{
		return !(*this == node);
	}

	/**
	 * @brief Возвращает количество детей узла.
	 * @return Количество детей.
	 */
	virtual unsigned char getChildrenCount() const
	{
		char childrenCount = 0;

		if (m_rightNode != 0)
			childrenCount++;

		if (m_leftNode != 0)
			childrenCount++;

		return childrenCount;
	}

	/**
	 * @brief Возвращает высоту узла.
	 * @return Высота узла.
	 */
	virtual unsigned char getHeight() const
	{
		return m_height;
	}

	/**
	 * @brief Возвращает указатель на левый дочерний узел.
	 * @return Указатель на левый дочерний узел.
	 */
	virtual X* getLeftNode()
	{
		return m_leftNode;
	}

	/**
	 * @brief Возвращает указатель на левый дочерний узел.
	 * @return Указатель на левый дочерний узел.
	 */
	virtual const X* getLeftNode() const
	{
		return m_leftNode;
	}

	/**
	 * @brief Возвращает ссылку на значение, хранимое в узле.
	 * @return Ссылка на значение, хранимое в узле.
	 */
	virtual ValueType& getPair()
	{
		return m_pair;
	}

	/**
	 * @brief Возвращает ссылку на значение, хранимое в узле.
	 * @return Ссылка на значение, хранимое в узле.
	 */
	virtual const ValueType& getPair() const
	{
		return m_pair;
	}

	/**
	 * @brief Возвращает ссылку на родительский узел
	 * @return Ссылка на родительский узел
	 */
	virtual X* getParentNode()
	{
		return m_parentNode;
	}

	/**
	 * @brief Возвращает ссылку на родительский узел
	 * @return Ссылка на родительский узел
	 */
	virtual const X* getParentNode() const
	{
		return m_parentNode;
	}

	/**
	 * @brief Возвращает указатель на правый дочерний узел.
	 * @return Указатель на равый дочерний узел.
	 */
	virtual X* getRightNode()
	{
		return m_rightNode;
	}

	/**
	 * @brief Возвращает указатель на правый дочерний узел.
	 * @return Указатель на правый дочерний узел.
	 */
	virtual const X* getRightNode() const
	{
		return m_rightNode;
	}

	/**
	 * @brief Указывает, является ли узел левым ребенком родителя.
	 * @return true, если узел является левым ребенком родителя, иначе false.
	 */
	virtual bool isLeftLinked() const
	{
		if (m_parentNode != 0)
		{
			if (this == m_parentNode->getLeftNode())
				return true;

			return false;
		}

		return false;
	}

	/**
	* @brief Указывает, является ли узел правым ребенком родителя.
	 * @return true, если узел является правым ребенком родителя, иначе false.
	 */
	virtual bool isRightLinked() const
	{
		if (m_parentNode != 0)
		{
			if (this == m_parentNode->getRightNode())
				return true;

			return false;
		}

		return false;
	}

	/**
	 * @brief Пересчитывает высоту.
	 * @details Пересчет высоты является необходимым после манипуляций над узлом, которые влияют
	 * на его местоположение в дереве.
	 */
	virtual void recalculateHeight()
	{
		unsigned char leftHeigth = 0;
		unsigned char rightHeigth = 0;

		if (m_leftNode != 0)
			leftHeigth = m_leftNode->getHeight();

		if (m_rightNode != 0)
			rightHeigth = m_rightNode->getHeight();

		if (leftHeigth > rightHeigth)
			m_height = leftHeigth + 1;
		else
			m_height = rightHeigth + 1;
	}

	/**
	 * @brief Устанавливает левого ребенка для текущего узла.
	 * @brief При установке левого ребенка для узла, текущий узел автоматически станет родителем
	 * левого ребенка.
	 * @param node Узел, который станет левым ребенком текущего узла.
	 */
	virtual void setLeftNode(X *node)
	{
		m_leftNode = node;

		if (node != 0)
			node->setParentNode(dynamic_cast<X*>(this));
	}

	/**
	 * @brief Устанавливает правого ребенка для текущего узла.
	 * @brief При установке правого ребенка для узла, текущий узел автоматически станет родителем
	 * правого ребенка.
	 * @param node Узел, который станет правым ребенком текущего узла.
	 */
	virtual void setRightNode(X *node)
	{
		m_rightNode = node;

		if (node != 0)
			node->setParentNode(dynamic_cast<X*>(this));
	}

protected:
	ValueType m_pair;
	X *m_parentNode, *m_leftNode, *m_rightNode;
	unsigned char m_height;

	/**
	 * @brief Устанавливает родителя для текущего узла.
	 * @param parentNode Узел, который станет родителем текущего узла.
	 */
	virtual void setParentNode(X *parentNode)
	{
		m_parentNode = parentNode;
	}
};
