#pragma once

#include "AVLTreeNode.h"

/**
 * @brief Абстрактный класс, представляющий базовую стратегию.
 * @tparam T Тип узла.
 * @tparam T1 Тип объекта-сравнивателя.
 */
template <typename T, typename T1>
class IStrategy
{
public:
	typedef T Node; ///< Тип узла.
	typedef T1 LessComparer; ///< Тип объекта-сравнивателя.
	typedef typename Node::KeyType KeyType; ///< Тип ключа.
	typedef typename Node::ValueType ValueType; ///< Тип значения.

	/**
	 * @brief Деструктор.
	 */
	virtual ~IStrategy()
	{

	}

	/**
	 * @brief Возвращает баланс узла.
	 * @details Баланс узла - разница между высотой правого поддерева и левого поддерева.
	 * @return Баланс узла.
	 */
	virtual int getNodeBalanceFactor(Node *node) const
	{
		if (node != 0)
			return getNodeHeigth(node->getRightNode()) - getNodeHeigth(node->getLeftNode());

		return 0;
	}

	/**
	 * @brief Возвращает высоту узла.
	 * @return Высота узла.
	 */
	virtual unsigned char getNodeHeigth(const Node *node) const
	{
		if (node != 0)
			return node->getHeight();

		return 0;
	}

	/**
	 * @brief Находит узел с минимальным ключем в поддереве.
	 * @param node Указатель на корень поддерева, в котором будет произведен поиск.
	 * @return В случае успешного поиска указатель на найденный узел, иначе указатель на корень
	 * поддерева.
	 */
	virtual Node *findMinKeyNode(Node *node)
	{
		if (node == 0 || node->getLeftNode() == 0)
			return node;

		return findMinKeyNode(node->getLeftNode());
	}

	/**
	 * @brief Находит узел с максимальным ключем в поддереве.
	 * @param node Указатель на корень поддерева, в котором будет произведен поиск.
	 * @return В случае успешного поиска указатель на найденный узел, иначе указатель на корень
	 * поддерева.
	 */
	virtual Node *findMaxKeyNode(Node *node)
	{
		if (node == 0 || node->getRightNode() == 0)
			return node;

		return findMaxKeyNode(node->getRightNode());
	}

	/**
	 * @brief Находит узел содержащий указанный ключ.
	 * @param node Указатель на корень поддерева, в котором будет произведен поиск.
	 * @param key Ключ, по которому будет произведен поиск.
	 * @return В случае успешного поиска указатель на найденный узел, иначе нулевой указатель.
	 */
	virtual Node* findNode(Node *node, const KeyType &key)
	{
		if (node == 0)
			return 0;

		//Корневой ключ равен запрошенному - нашли
		if (!m_less(node->getPair().first, key) && !m_less(key, node->getPair().first))
			return node;

		//Если запрошенный ключ больше корневого, ищем справа
		if (m_less(node->getPair().first, key))
			return findNode(node->getRightNode(), key);

		return findNode(node->getLeftNode(), key); //Иначе ищем слева
	}

	// Pure virtuals
	/**
	 * @brief Удаляет элемент из дерева по указанному ключу.
	 * @details Удаление произойдет только если ключ будет найден в дереве, в противном случае
	 * не будет выполнено никаких действий.
	 * @param rootNode Указатель на корневой узел поддерева, в котором будет произведено удаление.
	 * @param key Ключ, по которому будет произведено удаление.
	 * @param deleted Укажет, было ли произведено удаление.
	 * @return Указатель на новый корневой узел дерева.
	 */
	virtual Node* erase(Node *rootNode, const KeyType &key, bool &deleted) = 0;

	/**
	 * @brief Добавляет новый элемент в дерево.
	 * @param rootNode Указатель на корневой узел дерева.
	 * @param pair Значение, которое будет храниться в новом элементе.
	 * @return Указатель на новый корневой узел дерева.
	 */
	virtual Node* insert(Node *rootNode,  const ValueType &pair) = 0;

protected:
	LessComparer m_less;
};
