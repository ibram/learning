#include <algorithm>
#include <iostream>
#include <memory>
#include <string>

#include "AVLTreeStrategy.h"
#include "BinaryTree.h"
#include "RBTreeStrategy.h"

int getStrategy()
{
	int chosenStrategy = 0;

	do
	{
		std::cout << "Choose strategy to use:" << std::endl <<
				"\t1. AVL tree strategy." << std::endl <<
				"\t2. Red-black tree strategy." << std::endl <<
				"Enter number: ";
		std::cin >> chosenStrategy;
	}
	while(chosenStrategy != 1 && chosenStrategy != 2);

	return chosenStrategy;
}

int getItemsToAddCount()
{
	int itemsToAddCount = 0;

	do
	{
		std::cout << "How many items you want to add into the tree: ";
		std::cin >> itemsToAddCount;
	}
	while(itemsToAddCount < 1);

	return itemsToAddCount;
}

int getKeyToDelete()
{
	int keyToDelete = 0;
	std::cout << "Enter the key to delete from the tree: ";
	std::cin >> keyToDelete;

	return keyToDelete;
}

template<typename T, typename X, template<typename T1, typename X1, typename F> class A>
void processTree(BinaryTree<T, X, A> &binTree, int itemsToAddCount, int keyToDelete)
{
	for (int i = 0; i < itemsToAddCount; i++)
		binTree.insert(std::pair<int, std::string>(i, "item"));

	binTree.erase(keyToDelete);

	for (typename BinaryTree<T, X, A>::iterator i = binTree.begin(); i != binTree.end(); i++)
		std::cout << i->first << "_" << i->second << std::endl;
}

int main()
{
	int chosenStrategy = getStrategy();
	int itemsToAddCount = getItemsToAddCount();
	int keyToDelete = getKeyToDelete();

	if (chosenStrategy == 1)
	{
		std::auto_ptr<BinaryTree<int, std::string, AVLTreeStrategy> > binTree(
				new BinaryTree<int, std::string, AVLTreeStrategy>());
		processTree<int, std::string, AVLTreeStrategy>(*binTree, itemsToAddCount, keyToDelete);
		std::cout << "Tree size = " << binTree->size() << std::endl;
	}
	else
	{
		std::auto_ptr<BinaryTree<int, std::string, RBTreeStrategy> > binTree(
				new BinaryTree<int, std::string, RBTreeStrategy>());
		processTree<int, std::string, RBTreeStrategy>(*binTree, itemsToAddCount, keyToDelete);
		std::cout << "Tree size = " << binTree->size() << std::endl;
	}

	return 0;
}
